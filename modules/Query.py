from  Utils import DBaccess
import re

class Query(DBaccess):
    def __init__(self, database='trker_cmsr', login_type='login', verbose=False):
        super(Query, self).__init__(database, login_type, verbose)
   
    def get_locations(self):
        query = "SELECT l.* FROM {}.trkr_locations_v l".format(self.database)

        print(query)
        data = self.data_query(query)
        return data

    def get_condition_data_tables(self, pTableName = None):
        if pTableName is None:
            query = "SELECT c.* FROM {}.conditions c\
                    ORDER BY c.id".format(self.database)
        else:
            query = "SELECT c.* FROM {}.conditions c WHERE c.database_table='{}'".format(self.database,pTableName)

        print(query)
        data = self.data_query(query)  
        return data

    def get_part_data_tables(self, pPartName = None):
        if pPartName is None:
            query = "SELECT k.* FROM {}.kinds_of_part k".format(self.database)
        else:
            query = "SELECT k.* FROM {}.kinds_of_part k WHERE k.name='{}'".format(self.database,pPartName)

        print(query)
        data = self.data_query(query)
        return data

    def wire_bond_pull(self, module):
        table = self.get_condition_data_tables('TEST_WIREBOND_PULL')[0]["conditionTable"]
        query = "SELECT c.type_of_pull_area AS pull_loc, c.pull_position as pull_pos, c.force_corr_sensor_g AS force_sensor, c.force_corr_hybrid_g AS force_hybrid, c.force_measured_g AS measured_force\
                FROM {}.{} c \
                WHERE c.part_name_label='{}'".format(self.database, table, module)
        
        print(query.replace("  ",""))
        data = self.data_query(query)
        return data

    def get_ot_sensor_metrology(self, pModule , pGetLast = False):
        table = self.get_condition_data_tables('TEST_SENSOR_MTRLGY')[0]["conditionTable"]

        if pGetLast:
            query = "SELECT c.* \
                    FROM {}.{} c \
                    WHERE c.part_name_label='{}' \
                    ORDER BY c.condition_data_set_id DESC \
                    OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY".format(self.database,table,pModule)
        else:
            query = "SELECT c.* \
                    FROM {}.{} c \
                    WHERE c.part_name_label='{}'".format(self.database,table,pModule)
        
        print(query.replace("  ",""))
        data = self.data_query(query)
        return data


    def get_ot_module_metrology(self, pModule , pGetLast = False):
        table = self.get_condition_data_tables('TEST_MODULE_MTRLGY')[0]["conditionTable"]
        if pGetLast:
            query = "SELECT c.* \
                    FROM {}.{} c \
                    WHERE c.part_name_label='{}' \
                    ORDER BY c.condition_data_set_id DESC \
                    OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY".format(self.database,table,pModule)
        else:
            query = "SELECT c.* \
                    FROM {}.{} c \
                    WHERE c.part_name_label='{}'".format(self.database,table,pModule)
        
        print(query.replace("  ",""))
        data = self.data_query(query)
        return data
    
    def get_ot_module_iv(self, pModule , pGetLast = False):
        table = self.get_condition_data_tables('TEST_MODULE_IV')[0]["conditionTable"]

        if pGetLast:
            #Get highgest condidition data set id
            max_id_query = "SELECT c.condition_data_set_id \
                        FROM {}.{} c \
                        WHERE c.part_name_label='{}' \
                        ORDER BY c.condition_data_set_id DESC \
                        OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY".format(self.database,table, pModule)

            print(max_id_query.replace("  ",""))
            max_id = self.data_query(max_id_query)[0]["conditionDataSetId"]

            #get all entries with highest condition data set id
            query = "SELECT c.* \
                    FROM {}.{} c \
                    WHERE c.part_name_label='{}' \
                    AND c.condition_data_set_id={} \
                    ORDER BY c.condition_data_set_id DESC".format(self.database,table, pModule,max_id)
        else:
            query = "SELECT c.volts, c.condition_data_set_id as ccid, c.id AS cid, c.currnt_Namp, c.temp_Degc AS temp, c.time, d.run_type, d.run_number\
                    FROM {db}.{t} c \
                    INNER JOIN {db}.datasets d ON c.condition_data_set_id=d.ID\
                    WHERE c.part_name_label='{m}'".format(db=self.database,t=table,m=pModule)

        print(query.replace("  ",""))
        data = self.data_query(query)
        return data

    def get_ot_sensor_iv(self, pSensor , pGetLast = False):
        table = self.get_condition_data_tables('TEST_SENSOR_IV')[0]["conditionTable"]

        if pGetLast:
            #Get highgest condidition data set id
            max_id_query = "SELECT c.condition_data_set_id \
                        FROM {}.{} c \
                        WHERE c.part_name_label='{}' \
                        ORDER BY c.condition_data_set_id DESC \
                        OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY".format(self.database,table, pSensor)

            print(max_id_query.replace("  ",""))
            max_id = self.data_query(max_id_query)[0]["conditionDataSetId"]

            #get all entries with highest condition data set id
            query = "SELECT c.* \
                    FROM {}.{} c \
                    WHERE c.part_name_label='{}' \
                    AND c.condition_data_set_id={} \
                    ORDER BY c.condition_data_set_id DESC".format(self.database,table, pSensor,max_id)
        else:
            query = "SELECT c.volts, c.condition_data_set_id as ccid, c.id AS cid, c.currnt_Namp, c.temp_Degc AS temp, c.time, d.run_type, d.run_number\
                    FROM {db}.{t} c \
                    INNER JOIN {db}.datasets d ON c.condition_data_set_id=d.ID\
                    WHERE c.part_name_label='{s}'".format(db=self.database,t=table,s=pSensor)

        print(query.replace("  ",""))
        data = self.data_query(query)
        return data


    def get_ot_module_root_file_list( self, pModule, pGetLast = False):
        table = self.get_condition_data_tables('TEST_ROOT_FILES')[0]["conditionTable"]

        if pGetLast:
            #Get highgest condidition data set id
            max_id_query = "SELECT c.condition_data_set_id \
                        FROM {}.{} c \
                        WHERE c.part_name_label='{}' \
                        ORDER BY c.condition_data_set_id DESC \
                        OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY".format(self.database,table, pModule)

            print(max_id_query.replace("  ",""))
            max_id = self.data_query(max_id_query)[0]["conditionDataSetId"]

            #get all entries with highest condition data set id
            query = "SELECT c.* \
                    FROM {}.{} c \
                    WHERE c.part_name_label='{}' \
                    AND c.condition_data_set_id={} \
                    ORDER BY c.condition_data_set_id DESC".format(self.database,table, pModule,max_id)
        else:
            query = "SELECT c.* \
                    FROM {}.{} c \
                    WHERE c.part_name_label='{}'".format(self.database,table,pModule)

        print(query.replace("  ",""))
        data = self.data_query(query)
        return data

    def get_ot_2s_module_by_manufacturer(self, pManufacturer, pType="All"):
        info = {}

        tables = self.get_part_data_tables()

        module_table = next(table for table in tables if table["name"] == '2S Module')['partTable']

        query = "select pp.name_label AS module_name, pp.location\
                FROM {db}.parts part \
                INNER JOIN {db}.{mod_t} pp ON pp.ID = part.ID \
                WHERE pp.manufacturer='{assCenter}'".format( db      = self.database,
                                                        mod_t   = module_table,
                                                        assCenter  = pManufacturer)

        print(query.replace("  ",""))
        data = self.data_query(query)
        for dataPoint in data:
            if pType=="All" or pType=="all":
                info[dataPoint["moduleName"]]=dataPoint["location"]
            elif pType=="Prod" or pType=="prod":
                r = re.compile('2S_(?:18_5|18_6|40_6)_(?:KIT|AAC|NCP|FNL|IPG|BRN|NSR|BEL)-1[0-9]{4}')
                if r.match(dataPoint["moduleName"]):
                    info[dataPoint["moduleName"]]=dataPoint["location"]
            elif pType=="PreSeries" or pType=="preseries":
                r = re.compile('2S_(?:18_5|18_6|40_6)_(?:KIT|AAC|NCP|FNL|IPG|BRN|NSR|BEL)-01[0-9]{3}')
                if r.match(dataPoint["moduleName"]):
                    info[dataPoint["moduleName"]]=dataPoint["location"]
            elif pType=="KickOff" or pType=="kickoff":
                r = re.compile('2S_(?:18_5|18_6|40_6)_(?:KIT|AAC|NCP|FNL|IPG|BRN|NSR|BEL)-001[0-9]{2}')
                if r.match(dataPoint["moduleName"]):
                    info[dataPoint["moduleName"]]=dataPoint["location"]
            elif pType=="Proto" or pType=="proto":
                r = re.compile('2S_(?:18_5|18_6|40_6)_(?:KIT|AAC|NCP|FNL|IPG|BRN|NSR|BEL)-000[0-9]{2}')
                if r.match(dataPoint["moduleName"]):
                    info[dataPoint["moduleName"]]=dataPoint["location"]

        return [info]


    def get_ot_ps_module_by_manufacturer(self, pManufacturer, pType="All"):
        info = {}

        tables = self.get_part_data_tables()

        module_table = next(table for table in tables if table["name"] == 'PS Module'            )['partTable']

        query = "select pp.name_label AS module_name, pp.location\
                FROM {db}.parts part \
                INNER JOIN {db}.{mod_t} pp ON pp.ID = part.ID \
                WHERE pp.manufacturer='{assCenter}'".format( db      = self.database,
                                                        mod_t   = module_table,
                                                        assCenter  = pManufacturer)

        print(query.replace("  ",""))
        data = self.data_query(query)
        for dataPoint in data:
            if pType=="All" or pType=="all":
                info[dataPoint["moduleName"]]=dataPoint["location"]
            elif pType=="Prod" or pType=="prod":
                r = re.compile('PS_(?:16|26|40)_(?:IBA|IPG|BRN|FNL|DSY)-1[0-9]{4}')
                if r.match(dataPoint["moduleName"]):
                    info[dataPoint["moduleName"]]=dataPoint["location"]
            elif pType=="PreSeries" or pType=="preseries":
                r = re.compile('PS_(?:16|26|40)_(?:IBA|IPG|BRN|FNL|DSY)-01[0-9]{3}')
                q = re.compile('PS_(?:16|26|40)_(?:05|10)_(?:IBA|IPG|BRN|FNL|DSY)-01[0-9]{3}')
                if r.match(dataPoint["moduleName"]) or q.match(dataPoint["moduleName"]):
                    info[dataPoint["moduleName"]]=dataPoint["location"]
            elif pType=="KickOff" or pType=="kickoff":
                r = re.compile('PS_(?:16|26|40)_(?:IBA|IPG|BRN|FNL|DSY)-001[0-9]{2}')
                q = re.compile('PS_(?:16|26|40)_(?:05|10)_(?:IBA|IPG|BRN|FNL|DSY)-001[0-9]{2}')
                if r.match(dataPoint["moduleName"]) or q.match(dataPoint["moduleName"]):
                    info[dataPoint["moduleName"]]=dataPoint["location"]
            elif pType=="Proto" or pType=="proto":
                r = re.compile('PS_(?:16|26|40)_(?:IBA|IPG|BRN|FNL|DSY)-000[0-9]{2}')
                q = re.compile('PS_(?:16|26|40)_(?:05|10)_(?:IBA|IPG|BRN|FNL|DSY)-000[0-9]{2}')
                if r.match(dataPoint["moduleName"]) or q.match(dataPoint["moduleName"]):
                    info[dataPoint["moduleName"]]=dataPoint["location"]

        return [info]


    def get_ot_test_conditions_2s(self, pGetLast = False):
        if pGetLast:
            query = "SELECT c.* \
                    FROM {}.trkr_ot_2s_test_conditions_v c \
                    ORDER BY c.version DESC \
                    OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY".format(self.database)
        else:
            query = "SELECT c.* \
                    FROM {}.trkr_ot_2s_test_conditions_v c \
                    ORDER BY c.version DESC".format(self.database)
        print(query.replace("  ",""))
        data = self.data_query(query)
        return data

    def get_ot_test_conditions_ps(self, pGetLast = False):
        if pGetLast:
            query = "SELECT c.* \
                    FROM {}.trkr_ot_ps_test_conditions_v c \
                    ORDER BY c.version DESC \
                    OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY".format(self.database)
        else:
            query = "SELECT c.* \
                    FROM {}.trkr_ot_ps_test_conditions_v c \
                    ORDER BY c.version DESC".format(self.database)
        print(query.replace("  ",""))
        data = self.data_query(query)
        return data

    def get_ot_test_conditions_skeleton(self, pGetLast = False):
        if pGetLast:
            query = "SELECT c.* \
                    FROM {}.trkr_ot_skeleton_test_conditions_v c \
                    ORDER BY c.version DESC \
                    OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY".format(self.database)
        else:
            query = "SELECT c.* \
                    FROM {}.trkr_ot_skeleton_test_conditions_v c \
                    ORDER BY c.version DESC".format(self.database)
        print(query.replace("  ",""))
        data = self.data_query(query)
        return data

    def get_gipht_test_conditions( self ):
        query = "SELECT s.nevents_n AS S_NEVENTS_N, s.ot_Skeleton_Test_Conditions_Id AS S_OT_SKELETON_TEST_CONDITIONS, s.lv_v AS S_LV_V, s.version AS S_VERSION, \
                p.lv_v AS P_LV_V, \
                p.iv_naked_fine_stepsize_v AS P_IV_NAKED_FINE_STEPSIZE_V, p.iv_naked_coarse_stepsize_v AS P_IV_NAKED_COARSE_STEPSIZE_V, \
                p.iv_naked_fine_range_v AS P_IV_NAKED_FINE_RANGE_V, p.iv_naked_coarse_range_v AS P_IV_NAKED_COARSE_RANGE_V, \
                p.iv_encap_fine_stepsize_v AS P_IV_ENCAP_FINE_STEPSIZE_V, p.iv_encap_coarse_stepsize_v AS P_IV_ENCAP_COARSE_STEPSIZE_V, \
                p.iv_encap_fine_range_v AS P_IV_ENCAP_FINE_RANGE_V, p.iv_encap_coarse_range_v AS P_IV_ENCAP_COARSE_RANGE_V, \
                p.hv_encap_v AS P_HV_ENCAP_V, p.hv_naked_v AS P_HV_NAKED_V, p.iv_ps_readouts_n AS P_IV_PS_READOUTS_N, p.version AS P_VERSION, \
                p.nevents_n as P_NEVENTS_N, p.iv_settling_time_ms AS P_IV_SETTLING_TIME_MS, p.threshold_sigma AS P_THRESHOLD_SIGMA, \
                p.ot_ps_test_conditions_id AS P_OT_PS_TEST_CONDITIONS_ID, \
                t.lv_v AS T_LV_V, \
                t.iv_naked_fine_stepsize_v AS T_IV_NAKED_FINE_STEPSIZE_V, t.iv_naked_coarse_stepsize_v AS T_IV_NAKED_COARSE_STEPSIZE_V, \
                t.iv_naked_fine_range_v AS T_IV_NAKED_FINE_RANGE_V, t.iv_naked_coarse_range_v AS T_IV_NAKED_COARSE_RANGE_V, \
                t.iv_encap_fine_stepsize_v AS T_IV_ENCAP_FINE_STEPSIZE_V, t.iv_encap_coarse_stepsize_v AS T_IV_ENCAP_COARSE_STEPSIZE_V, \
                t.iv_encap_fine_range_v AS T_IV_ENCAP_FINE_RANGE_V, t.iv_encap_coarse_range_v AS T_IV_ENCAP_COARSE_RANGE_V, \
                t.hv_encap_v AS T_HV_ENCAP_V, t.hv_naked_v AS T_HV_NAKED_V, t.iv_ps_readouts_n AS T_IV_PS_READOUTS_N, t.version AS T_VERSION, \
                t.nevents_n as T_NEVENTS_N, t.iv_settling_time_ms AS T_IV_SETTLING_TIME_MS, t.threshold_sigma AS T_THRESHOLD_SIGMA, \
                t.ot_2s_test_conditions_id AS T_OT_2S_TEST_CONDITIONS_ID\
                FROM {db}.trkr_ot_skeleton_test_conditions_v s, {db}.trkr_ot_ps_test_conditions_v p , {db}.trkr_ot_2s_test_conditions_v t \
                ORDER BY s.version DESC , p.version DESC , t.version DESC\
                OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY".format(db=self.database)
        print(query.replace("  ",""))
        data = self.data_query(query)
        return data

    def get_ot_module_information_2s(self, pModule):     
        info = {"fehLSerial": "",
                "fehRSerial": "",
                "topSensorBarcode": "",
                "bottomSensorBarcode": ""}

        tables = self.get_part_data_tables()

        module_table    = next(table for table in tables if table["name"] == '2S Module'            )['partTable']
        sensor_table    = next(table for table in tables if table["name"] == '2S Sensor'            )['partTable']
        feh_table       = next(table for table in tables if table["name"] == '2S Front-end Hybrid'  )['partTable']
        seh_table       = next(table for table in tables if table["name"] == '2S Service Hybrid'    )['partTable']
        vtrx_table       = next(table for table in tables if table["name"] == 'VTRx+'    )['partTable']


        query = "SELECT pp.name_label AS module_name, pp.location, pp.description, \
                pp.a2s_sensor_spacing AS spacing, pp.astatus AS module_status, pp.aposition_index AS position_index, \
                pp.aring_index AS ring_index, pp.amodule_integration_status AS module_integration_status, pp.acontact_points AS contact_points, \
                part.manufacturer, \
                s.a2s_sensor_posn AS sensor_position, s.name_label AS sensor_name_label, s.barcode AS sensor_barcode, \
                feh.afe_hybrid_side AS feh_hybrid_side, feh.serial_number AS feh_serial, \
                seh.serial_number AS seh_serial, \
                v.name_label AS vtrx_name_label, \
                r2.child_serial_number AS lpGBT_serial_number \
                FROM {db}.parts part \
                INNER JOIN {db}.{mod_t} pp ON pp.ID = part.ID \
                LEFT JOIN {db}.trkr_relationships_v r ON r.parent_name_label=pp.name_label \
                LEFT JOIN {db}.{feh_t} feh ON r.child_serial_number=feh.serial_number \
                LEFT JOIN {db}.{seh_t} seh ON r.child_serial_number=seh.serial_number \
                LEFT JOIN {db}.{s_t} s ON r.child_name_label=s.name_label \
                LEFT JOIN {db}.{v_t} v ON r.child_name_label=v.name_label \
                LEFT JOIN {db}.trkr_relationships_v r2 ON r2.parent_serial_number=seh.serial_number \
                WHERE pp.name_label='{module}'".format( db      = self.database,
                                                        mod_t   = module_table,
                                                        feh_t   = feh_table,
                                                        seh_t   = seh_table,
                                                        s_t     = sensor_table,
                                                        v_t     = vtrx_table,
                                                        module  = pModule)

        print(query.replace("  ",""))
        data = self.data_query(query)
        #print(data)
        for dataPoint in data:
            info.update(dataPoint)
            info["fehLSerial"] = dataPoint["fehSerial"] if dataPoint.get("fehHybridSide","") == "Left"  else info["fehLSerial"] 
            info["fehRSerial"] = dataPoint["fehSerial"] if dataPoint.get("fehHybridSide","") == "Right" else info["fehRSerial"] 
            info["topSensorBarcode"]    = dataPoint["sensorBarcode"] if dataPoint.get("sensorPosition","") == "TOP"    else info["topSensorBarcode"] 
            info["bottomSensorBarcode"] = dataPoint["sensorBarcode"] if dataPoint.get("sensorPosition","") == "BOTTOM" else info["bottomSensorBarcode"] 

        info.pop("fehHybridSide", None)
        info.pop("sensorPosition", None)
        info.pop("fehSerial", None)
        info.pop("sensorBarcode", None)
        if len(data)==0:
            return {}
        else:
            return [info]
        
    def get_ot_module_information_ps(self, pModule):
        info = {"fehLSerial": "",
                "fehRSerial": ""}

        tables = self.get_part_data_tables()

        module_table = next(table for table in tables if table["name"] == 'PS Module'            )['partTable']
        feh_table    = next(table for table in tables if table["name"] == 'PS Front-end Hybrid'  )['partTable']
        poh_table    = next(table for table in tables if table["name"] == 'PS Service Hybrid'    )['partTable']
        roh_table    = next(table for table in tables if table["name"] == 'PS Read-out Hybrid'   )['partTable']
        psp_table    = next(table for table in tables if table["name"] == 'PS-p Sensor'          )['partTable']
        pss_table    = next(table for table in tables if table["name"] == 'PS-s Sensor'          )['partTable']
        vtrx_table   = next(table for table in tables if table["name"] == 'VTRx+'                )['partTable']
        mapsa_table  = next(table for table in tables if table["name"] == 'MaPSA'                )['partTable']
        mpa_table    = next(table for table in tables if table["name"] == 'MPA Chip'             )['partTable']


        query = "select pp.name_label AS module_name, pp.location, pp.description, \
                part.manufacturer, \
                pp.aps_sensor_spacing AS spacing, pp.astatus AS module_status, pp.aposition_index AS position_index, \
                pp.aring_index AS ring_index, pp.amodule_integration_status AS module_integration_status, \
                pss.name_label AS pss_name_label, \
                map.name_label AS mapsa_name_label, \
                psp.name_label AS psp_name_label, \
                feh.afe_hybrid_side AS feh_hybrid_side, feh.serial_number AS feh_serial, \
                roh.serial_number AS roh_serial, roh.alpgbt_bandwidth AS lpgbt_bandwidth, \
                poh.serial_number AS poh_serial, \
                v.name_label AS vtrx, \
                r3.child_serial_number AS lpGBT_serial_number \
                FROM {db}.parts part \
                INNER JOIN {db}.{mod_t} pp ON pp.ID = part.ID \
                LEFT JOIN {db}.trkr_relationships_v r ON r.parent_name_label=pp.name_label \
                LEFT JOIN {db}.{feh_t} feh ON r.child_serial_number=feh.serial_number \
                LEFT JOIN {db}.{roh_t} roh ON r.child_serial_number=roh.serial_number \
                LEFT JOIN {db}.{poh_t} poh ON r.child_serial_number=poh.serial_number \
                LEFT JOIN {db}.{pss_t} pss ON r.child_name_label=pss.name_label \
                LEFT JOIN {db}.{map_t} map ON r.child_name_label=map.name_label \
                LEFT JOIN {db}.{v_t}   v   ON r.child_name_label=v.name_label \
                LEFT JOIN {db}.trkr_relationships_v r2 ON r2.parent_name_label=map.name_label \
                LEFT JOIN {db}.{psp_t} psp ON r2.child_name_label=psp.name_label \
                LEFT JOIN {db}.trkr_relationships_v r3 ON r3.parent_serial_number=roh.serial_number \
                WHERE pp.name_label='{module}'".format( db      = self.database,
                                                        mod_t   = module_table,
                                                        feh_t   = feh_table,
                                                        roh_t   = roh_table,
                                                        poh_t   = poh_table,
                                                        pss_t   = pss_table,
                                                        psp_t   = psp_table,
                                                        map_t   = mapsa_table,
                                                        v_t     = vtrx_table,
                                                        module  = pModule)

        print(query.replace("  ",""))
        data = self.data_query(query)
        for dataPoint in data:
            info.update(dataPoint)
            info["fehLSerial"] = dataPoint["fehSerial"] if dataPoint.get("fehHybridSide","")=="Left" else info["fehLSerial"] 
            info["fehRSerial"] = dataPoint["fehSerial"] if dataPoint.get("fehHybridSide","")=="Right" else info["fehRSerial"]

        info.pop("fehHybridSide", None)
        info.pop("fehSerial", None)

        mapsa = info.get("mapsaNameLabel","") 
        if mapsa != "":
            query = "SELECT r.child_name_label AS name_label, mpa.serial_number, mpa.achip_posn_on_sensor AS posn_on_sensor\
                    FROM {db}.trkr_relationships_v r \
                    INNER JOIN {db}.{mpa_t} mpa ON mpa.ID = r.child_id \
                    WHERE r.parent_name_label='{mapsa}'".format( db      = self.database,
                                                            psp_t   = psp_table,
                                                            map_t = mapsa_table,
                                                            mpa_t = mpa_table,
                                                            mapsa = mapsa)
            print(query.replace("  ",""))
            psp = self.data_query(query)
            info["MPAs"]=psp

        if len(data)==0:
            return {}
        else:
            return [info]

    def get_ot_boxes( self ):
        query = "SELECT p.name_label, l.location_name\
                 FROM {db}.parts p \
                INNER JOIN {db}.trkr_locations_v l ON l.location_id=p.location_id \
                WHERE p.kind_of_part_id=14600".format(db=self.database)
        print(query)
        data = self.data_query(query)
        locations = {}
        for dataPoint in data:
            locations[dataPoint["locationName"]]=0

        for dataPoint in data:
            locations[dataPoint["locationName"]] +=1

        [print(location,"\t",number) for location, number in locations.items()]

        return data

    def getMpaIdsOfWafer(self, lot_name , wafer_number):
        query = "SELECT p.id, p.name_label, p.serial_number \
                FROM {db}.parts p \
                LEFT JOIN {db}.p11420 mpa ON mpa.id=p.id \
                WHERE p.kind_of_part_id=11420 AND SUBSTR(p.name_label,1,9)='{lot}_{wafer}'   ".format(db=self.database, lot=lot_name, wafer=wafer_number)
        data = self.data_query(query)
        return data

    def getSsaIdsOfWafer(self, lot_name , wafer_number):
        offset = 0
        limit = 1000
        data = []
        digits = 9
        kop_id = 12220 # 19840 #
        if int(wafer_number)>=10:
            digits=10
        while True:
            query = "SELECT p.id, p.name_label, p.serial_number \
                    FROM {db}.parts p \
                    LEFT JOIN {db}.p{kop} ssa ON ssa.id=p.id \
                    WHERE p.kind_of_part_id={kop} AND SUBSTR(p.name_label,0,{len})='{lot}_{wafer}_' \
                    OFFSET {off} ROWS FETCH NEXT {lim} ROWS ONLY".format(db=self.database,
                                                                        off = offset,
                                                                        kop = kop_id,
                                                                        lot=lot_name,
                                                                        wafer=wafer_number,
                                                                        lim = limit,
                                                                        len =digits)
            print(query)
            dat = self.data_query(query)
            data.extend(dat)
            offset += 1000
            if dat== []:
                break
        print(len(data))
        return data

    def getCicIdsOfWafer(self, lot_name , wafer_number):
        offset = 0
        limit = 1000
        data = []
        digits = 9
        kop_id = 12260 # 19840 #
        if int(wafer_number)>=10:
            digits=10
        while True:
            query = "SELECT p.id, p.name_label, p.serial_number \
                    FROM {db}.parts p \
                    LEFT JOIN {db}.p{kop} cic ON cic.id=p.id \
                    WHERE p.kind_of_part_id={kop} AND SUBSTR(p.name_label,0,{len})='{lot}_{wafer}_' \
                    OFFSET {off} ROWS FETCH NEXT {lim} ROWS ONLY".format(db=self.database,
                                                                        off = offset,
                                                                        kop = kop_id,
                                                                        lot=lot_name,
                                                                        wafer=wafer_number,
                                                                        lim = limit,
                                                                        len =digits)
            print(query)
            dat = self.data_query(query)
            data.extend(dat)
            offset += 1000
            if dat== []:
                break
        print(len(data))
        return data



    def get_uncorrected_vtrx_ids( self ):
        tables = self.get_part_data_tables()

        vtrx_table   = next(table for table in tables if table["name"] == 'VTRx+')['partTable']
        kop = int(vtrx_table[1:])
        query = "select part.id, part.name_label, part.serial_number, part.barcode \
                FROM {db}.parts part \
                WHERE part.kind_of_part_id={kop}\
                AND SUBSTR(part.barcode, 0, 5)='16611'\
                OFFSET 0 ROWS FETCH NEXT 5 ROWS ONLY".format(db=self.database, kop =kop)#vtrx_t = vtrx_table)
        data = self.data_query(query)
        print(data)

        return data

    def get_ps_roh_no_attribute( self ):
        tables = self.get_part_data_tables()
        roh_table    = next(table for table in tables if table["name"] == 'PS Read-out Hybrid'   )['partTable']

        query = "select part.id, part.serial_number, roh.alpgbt_bandwidth, roh.aps_sensor_spacing, roh.alpgbt_version \
                FROM {db}.parts part \
                INNER JOIN {db}.{roh_t} roh ON roh.serial_number=part.serial_number \
                WHERE roh.alpgbt_bandwidth IS NULL OR roh.aps_sensor_spacing IS NULL OR roh.alpgbt_version IS NULL\
                OFFSET 0 ROWS FETCH NEXT 350 ROWS ONLY".format(db=self.database, roh_t = roh_table)
        data = self.data_query(query)
        return data

    def get_seh_info( self, seh ): 
        tables = self.get_part_data_tables()
        #roh_table    = next(table for table in tables if table["name"] == 'PS Read-out Hybrid'   )['partTable']
        seh_table    = next(table for table in tables if table["name"] == '2S Service Hybrid'   )['partTable']

        query = "SELECT part.*, r.*\
                FROM {db}.parts part\
                INNER JOIN {db}.{seh_t} seh ON seh.id=part.id \
                LEFT JOIN {db}.trkr_relationships_v r ON r.parent_serial_number=seh.serial_number \
                WHERE seh.serial_number='{hybrid_sn}'". format(db = self.database,seh_t = seh_table, hybrid_sn=seh)
        print(query)

        data = self.data_query(query)
        return data


    def get_roh_info( self, roh): 
        tables = self.get_part_data_tables()
        roh_table    = next(table for table in tables if table["name"] == 'PS Read-out Hybrid'   )['partTable']

        query = "SELECT part.*, r.*\
                FROM {db}.parts part\
                INNER JOIN {db}.{roh_t} roh ON roh.id=part.id \
                LEFT JOIN {db}.trkr_relationships_v r ON r.parent_serial_number=roh.serial_number \
                WHERE roh.serial_number='{hybrid_sn}'". format(db = self.database,roh_t = roh_table, hybrid_sn=roh)
        print(query)

        data = self.data_query(query)
        return data



    def test( self ):
        #return self.mpawafer()

        return self.get_module_test_status()
        tables = self.get_part_data_tables()
        cond = self.get_condition_data_tables()
        #[print(c) for c in cond]
        #print(tables)

        s2_table    = next(table for table in tables if table["name"] == '2S Module'   )['partTable']
        ps_table    = next(table for table in tables if table["name"] == 'PS Module'   )['partTable']

        pull_table    = next(c for c in cond if c["name"] == 'Wire-Bond Pull Test'   )['conditionTable']
        metro_table    = next(c for c in cond if c["name"] == 'Tracker Module Metrology'   )['conditionTable']
        iv_table    = next(c for c in cond if c["name"] == 'Tracker Module IV Test'   )['conditionTable']
        root_table    = next(c for c in cond if c["name"] == 'Module Analysis Root Files'   )['conditionTable']


        query = "SELECT part.*\
                FROM {db}.parts part\
                LEFT JOIN {db}.p11420 mpa ON mpa.id=part.id \
                WHERE part.name_label='N6Y215_04_310'"
        query = "SELECT p.* \
                FROM {db}.parts p \
                WHERE p.id=910654".format(db=self.database)
        query = "SELECT p.*,d.*, t.*\
                FROM trker_cmsr.parts p\
                INNER JOIN (\
                    SELECT t.PART_ID, MAX(t.BEGIN_DATE) AS CORRECT_PRODUCTION_DATE\
                    FROM {db}.datasets t WHERE\
                    t.KIND_OF_CONDITION_ID=9400 AND\
                    t.RUN_TYPE='mod_final'\
                    GROUP BY t.PART_ID\
                ) d ON p.ID = d.PART_ID\
                WHERE p.KIND_OF_PART in ('2S Module', 'PS Module')\
                    AND p.NAME_LABEL LIKE '2S_18_6_KIT-1%'".format(db=self.database)

        query = "SELECT p.name_label \
                FROM {db}.parts p \
                LEFT JOIN {db}.{s2} mod ON mod.id = p.id \
                WHERE p.NAME_LABEL LIKE '2S_%-1000%' ".format(db=self.database,s2=s2_table)


        query = "select metro.id AS metroID, iv.id AS ivID, p.name_label \
                FROM {db}.parts p \
                LEFT JOIN {db}.{m} metro ON metro.part_id = p.id \
                LEFT JOIN {db}.{iv} iv ON iv.part_id = p.id \
                WHERE p.NAME_LABEL='2S_18_5_KIT-10001' ".format(db=self.database,
                                                                m=metro_table,
                                                                iv=iv_table)
        query = "select t.run_type, t.kind_of_condition, t.INSERTION_USER \
                FROM {db}.datasets t \
                LEFT JOIN {db}.parts p ON p.id=t.part_id \
                WHERE p.name_label='2S_18_5_KIT-10001' ".format(db=self.database,
                                                                m=metro_table,
                                                                iv=iv_table)



        data = self.data_query(query)
        #[print(d["nameLabel"],d["correctProductionDate"]) for d in data]
        return data

    def mpawafer ( self ):
        cond = self.get_condition_data_tables()
        wafer    = next(c for c in cond if c["name"] == 'MPA Wafer Probe Results'   )['conditionTable']

        query = "select w.* \
                FROM {db}.{w} w".format(db=self.database,
                                        w=wafer)

        data = self.data_query(query)
        return data

    def get_module_test_status( self ):

        query = "select p.name_label \
                FROM {db}.parts p  \
                WHERE p.NAME_LABEL LIKE '2S_%-1000%' OR p.NAME_LABEL LIKE 'PS_%-1000%'".format(db=self.database)
        data = self.data_query(query)
        modules = []
        for dataPoint in data:
            modules.append(dataPoint["nameLabel"])


        query = "select p.name_label, t.RUN_TYPE, t.KIND_OF_CONDITION, t.INSERTION_USER, t.INSERTION_TIME \
                FROM {db}.parts p  \
                JOIN {db}.datasets t ON p.id=t.part_id\
                WHERE p.NAME_LABEL LIKE '2S_%-1000%' OR p.NAME_LABEL LIKE 'PS_%-1000%'".format(db=self.database)
        data = self.data_query(query)
        print("Module name \t Metrology \t Pull Test \t Module IV \t Module Test")

        for dataPoint in data:
            modules.append(dataPoint["nameLabel"])

        modules = list(set(modules))
        modules = sorted (modules)

        for module in modules:
            module_summary = {'Tracker Module Metrology': "", 'Wire-Bond Pull Test': '', 'Tracker Module IV Test': "",'Module Analysis Root Files':""}

            for dataPoint in data:
                if dataPoint["nameLabel"] == module:
                    for key in module_summary:
                        if dataPoint.get("kindOfCondition","") == key:
                            module_summary[key] = "x"

            m_string = ""
            if "PS" in module:
                m_string = "\t"

            for key in module_summary:
                m_string += module_summary[key] + "\t\t"

            print(module + "\t" +  m_string)

        #pull_table    = next(c for c in cond if c["name"] == 'Wire-Bond Pull Test'   )['conditionTable']
        #metro_table    = next(c for c in cond if c["name"] == 'Tracker Module Metrology'   )['conditionTable']
        #iv_table    = next(c for c in cond if c["name"] == 'Tracker Module IV Test'   )['conditionTable']
        #root_table    = next(c for c in cond if c["name"] == 'Module Analysis Root Files'   )['conditionTable']


        return data
