#!/usr/bin/python
# $Id$
# Created by Stefan Maier <s.maier@kit.edu>, 22-January-2024

# This module contains the classes that produce the XML file for uploading
# the OT Module Metrology data.

from BaseUploader import ConditionData
from shutil       import copyfile
from copy         import deepcopy
from Utils        import search_files, check_ot_module_label
from Exceptions   import *


class OTLadderThermalQCFile(ConditionData):
   """Produces the XML file to upload OT Module Test root file."""

   data_description  = {    'kind_of_part' : 'Ladder',     
                            'cond_name'    : 'Ladder Thermal QC',
                            'table_name'   : 'TEST_LADDER_THRML_QC',
                            'DBvar_vs_TxtHeader' : {  'THERMAL_QC_FILE'  : 'THERMAL_QC_FILE',
                                                      'THERMAL_QC_DATE'  : 'THERMAL_QC_DATE'},
                            'mandatory' : []}
   def __init__(self, pRunDataReader, pMeasurementDataReader=None):

      """Constructor: it requires configuration of the upload and the measurement data
      pRunDataReader:
         Serial
         Date
         Commen
         Location
         Inserter
      pMeasurementDataReader:
         THERMAL_QC_FILE
         THERMAL_QC_DATE
      """

      ladder_serial = pRunDataReader.getDataFromTag('#Serial')
      location    = pRunDataReader.getDataFromTag('#Location')
      comment     = pRunDataReader.getDataFromTag('#Comment')
      inserter    = pRunDataReader.getDataFromTag('#Inserter')
      date        = pRunDataReader.getDataFromTag("#Date")
      run_type    = pRunDataReader.getDataFromTag("#RunType")
      
      configuration = {}
      data_description = self.data_description

      configuration.update(deepcopy(data_description))
      configuration['inserter']     = inserter
      configuration['serial']       = ladder_serial
      configuration['run_comment']  = comment
      configuration['data_comment'] = comment
      configuration['run_location'] = location
      configuration['run_begin']    = date
      configuration['run_type']     = run_type
      configuration['data_version'] = "1.0"
      configuration['attributes']   = [['Data Quality','Good']]  

         #Check if the data provided could cause harm during upload
      name = 'DATA-{}'.format(ladder_serial)
      configuration.update(self.data_description)
      ConditionData.__init__(self, name, configuration, pMeasurementDataReader)