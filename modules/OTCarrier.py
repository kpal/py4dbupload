#!/usr/bin/python
# $Id$
# Created by Andreas Mussgiller <andreas.mussgiller@cern.ch>, 22-Feb-2025

# This module contains the classes that produce the XML file for uploading
# the 2S and PS module carrier components.

from BaseUploader import BaseUploader, ConditionData
from lxml         import etree
from shutil       import copyfile
from copy         import deepcopy
from Exceptions   import *
from Utils        import search_files
from enum         import Enum
from datetime     import datetime
import sys

class OTCarrier():
    """Class to decode the barcode given to the module carrier."""
    
    def __init__(self, barcode):
        """Constructor: it requires the barcode that is used to identify the component."""
        self.barcode = barcode
        self.serial = barcode
        self.name_label = barcode
        
        try:
            self.type = barcode.split('-')[0]
            self.b_kind = '2S Carrier' if self.type=='2S' else 'PS Carrier'
        except:
            self.type = None
            self.b_kind = None
      
        try:
            manufacturercode = barcode.split('-')[1]
            if manufacturercode == 'AAC':
                self.manufacturer = 'RWTH Aachen'
            elif manufacturercode == 'FNL':
                self.manufacturer = 'Fermilab'
            elif manufacturercode == 'WSU':
                self.manufacturer = 'Wayne State University'
            elif manufacturercode == 'BRN':
                    self.manufacturer = "Brown University"
            elif manufacturercode == 'DSY':
                self.manufacturer = "DESY"               
            else:
                self.manufacturer = None
        except:
            self.manufacturer = None



class OTCarrierComponentT(OTCarrier,BaseUploader):
    """Produces the XML file to register any baseplate components.""" 

    def __init__(self, barcode, location, date, comment=None, inserter=None):
        """Constructor for component registration: it requires the barcode 
           of the baseplate component, the component location, the manufacturer 
           and the production date to produce the configuration dictionary. 
           Optional parameneters are:
             comment:      description of the component.
             inserter:     person recording the data (override the CERN user name)
        """
        OTCarrier.__init__(self,barcode)

        name = self.name_label
        cdict = {}
        
        cdict['kind_of_part']    = self.b_kind   
        cdict['unique_location'] = location
        cdict['manufacturer']    = self.manufacturer
        cdict['product_date']    = date
        cdict['serialNumber']    = self.barcode
        cdict['barcode']         = self.barcode
        cdict['nameLabel']       = self.name_label
        # cdict['version']         = 'Empty'    Should we define a version string?
  
        if comment!=None: cdict['description'] = comment
        if inserter!=None: cdict['inserter'] = inserter
  
        BaseUploader.__init__(self, name, cdict)
        
        self.mandatory += ['manufacturer','kind_of_part','unique_location']


    def load_attribute(self, name, value):
        """Load a component attribute. Check the allowed attributes."""
        try:
           allowed = {}
           for a in self.attributes:
              allowed[a['attributeName']] = a['allowedValues'].split(',')
        except:
           allowed = None
              
        if allowed!=None:
           if name not in allowed.keys():
              raise BadParameters(self.__class__.__name__,f'{name} is not an attribute of {self.b_kind}.')
        
           allowed_values = allowed[name]
           if value not in allowed_values:
              raise BadParameters(self.__class__.__name__,f'{value} is not allowed for {name} attribute.')
        
        attributes = self.retrieve('attributes')
        
        try:
           set_attributes = [a for a in attributes if a[0]!=name]
        except:
           set_attributes = []
           
        set_attributes.append((name,value))
        self.configuration['attributes']=set_attributes


    def xml_builder(self,parts):
        """Process data."""
        part = None
  
        from Utils import DBaccess
        #db = DBaccess(database=f'trker_{self.database}')
        part_id = self.db.component_id(self.name_label,kop=self.retrieve('kind_of_part'))
        if part_id!=None:
            pass
            # registered
            #part = etree.SubElement(parts, "PART", mode="auto")
            #etree.SubElement(part,"KIND_OF_PART").text = self.b_kind
            #etree.SubElement(part,"SERIAL_NUMBER").text = self.serial
        else:
            # not registered
            part = self.build_parts_on_xml(parts,
                                           serial=self.serial,
                                           name_label=self.name_label,
                                           barcode=self.barcode)

    def dump_xml_data(self, filename=''):
        """Writes the hybrid data in the XML file for the database upload."""
        inserter  = self.retrieve('inserter')
        root      = etree.Element("ROOT")
        if inserter is not None:
            root.set('operator_name',inserter)
        
        part_tree = etree.SubElement(root,"PARTS")
  
        self.xml_builder(part_tree)
  
        if filename == '':
            filename = '%s.xml'%self.name
        with open(filename, "w") as f:
            f.write(etree.tostring(root, pretty_print = True,
                                   xml_declaration = True,
                                   standalone="yes").decode('UTF-8'))
        return filename
