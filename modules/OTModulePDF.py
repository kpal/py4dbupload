import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import qrcode
import numpy as np
import matplotlib.pyplot as plt
from reportlab.lib import colors
from BaseUploader import BaseUploader

class OTModulePDF(BaseUploader):
#intitialization
    def __init__(self, mod_dir):
        self.module_dir = mod_dir     
    
#main functions to collect, order and extract the important information out of the query data
    #sort and order name labels, create the qr codes thereof and get general information about the module
    def names_qrImage(self, data):
        df = pd.DataFrame(data)
        required_columns = ['moduleName', 'fehLSerial', 'fehRSerial', 'topSensorBarcode', 'bottomSensorBarcode', 'sehSerial','vtrxNameLabel',  'moduleStatus', 'description', 'spacing', 'manufacturer']
        #ensure each required column exists in DataFrame, initializing missing ones with NaN
        for col in required_columns:
            df[col] = df.get(col, np.nan)

        #extracting names out of DataFrame
        module, l_feh, r_feh, t_sensor, b_sensor, sh,vtrx, status, comments, spacing, manufacturer = df.iloc[0][required_columns]
        
        qr_code_mapping =   {
                            'moduleName': 'mod_qr.jpeg',
                            'fehLSerial': 'lfeh_qr.jpeg',
                            'fehRSerial': 'rfeh_qr.jpeg',
                            'sehSerial': 'sh_qr.jpeg',
                            'vtrxNameLabel': 'vtrx_qr.jpeg',
                            'topSensorBarcode': 'tsen_qr.jpeg',
                            'bottomSensorBarcode': 'bsen_qr.jpeg'
                            }

        #generate qr codes 
        for col, file_name in qr_code_mapping.items():
            if pd.notna(df[col][0]):
                self.generate_qr_code(df[col][0], f"{self.module_dir}/{file_name}")

        '''if 'fehLSerial' not in df.columns:          df['fehLSerial'] = np.nan
        if 'fehRSerial' not in df.columns:          df['fehRSerial'] = np.nan
        if 'topSensorBarcode' not in df.columns:    df['topSensorBarcode'] = np.nan
        if 'bottomSensorBarcode' not in df.columns: df['bottomSensorBarcode'] = np.nan
        if 'sehSerial' not in df.columns:           df['sehSerial'] = np.nan
        if 'moduleStatus' not in df.columns:        df['moduleStatus'] = np.nan
        if 'description' not in df.columns:         df['description'] = np.nan
        if 'spacing' not in df.columns:             df['spacing'] = np.nan               
        if 'manufacturer' not in df.columns:        df['manufacturer'] = np.nan   

        module      = df['moduleName'][0]
        l_feh       = df['fehLSerial'][0]
        r_feh       = df['fehRSerial'][0]
        t_sensor    = df['topSensorBarcode'][0]
        b_sensor    = df['bottomSensorBarcode'][0]
        sh          = df['sehSerial'][0]
        vtrx        = df['vtrxNameLabel'][0]
        status      = df['moduleStatus'][0]
        comments    = df['description'][0]
        spacing     = df['spacing'][0]
        manufacturer= df['manufacturer'][0]

        #create a qr code of the parts, if they are built-in
        pd.notna(df['parentNameLabel'][0]) and self.generate_qr_code(df['parentNameLabel'][0], f"{self.module_dir}/mod_qr.jpeg")
        pd.notna(df['fehLSerial'][0]) and self.generate_qr_code(df['fehLSerial'][0], f"{self.module_dir}/lfeh_qr.jpeg")
        pd.notna(df['fehRSerial'][0]) and self.generate_qr_code(df['fehRSerial'][0], f"{self.module_dir}/rfeh_qr.jpeg")
        pd.notna(df['sehSerial'][0]) and self.generate_qr_code(df['sehSerial'][0], f"{self.module_dir}/sh_qr.jpeg")
        pd.notna(df['topSensorBarcode'][0]) and self.generate_qr_code(df['topSensorBarcode'][0], f"{self.module_dir}/tsen_qr.jpeg")
        pd.notna(df['bottomSensorBarcode'][0]) and self.generate_qr_code(df['bottomSensorBarcode'][0],  f"{self.module_dir}/bsen_qr.jpeg")
'''
        return module, l_feh, r_feh, t_sensor, b_sensor, sh, vtrx,status, comments, spacing, manufacturer

    #sort and order dicing information
    def dicing(self, data_t, data_b):
        positions = top = bottom = []

        if data_b and data_t:
            df_sen_mtrlgy_t = pd.DataFrame(data_t)
            df_sen_mtrlgy_b = pd.DataFrame(data_b)

            t_eastl     = df_sen_mtrlgy_t['distanceLeftUm'].iloc[0]
            t_northl    = df_sen_mtrlgy_t['distanceLeftUm'].iloc[1]
            t_westl     = df_sen_mtrlgy_t['distanceLeftUm'].iloc[2]
            t_southl    = df_sen_mtrlgy_t['distanceLeftUm'].iloc[3]

            t_eastr     = df_sen_mtrlgy_t['distanceRightUm'].iloc[0]
            t_northr    = df_sen_mtrlgy_t['distanceRightUm'].iloc[1]
            t_westr     = df_sen_mtrlgy_t['distanceRightUm'].iloc[2]
            t_southr    = df_sen_mtrlgy_t['distanceRightUm'].iloc[3]

            b_eastl     = df_sen_mtrlgy_b['distanceLeftUm'].iloc[0]
            b_northl    = df_sen_mtrlgy_b['distanceLeftUm'].iloc[1]
            b_westl     = df_sen_mtrlgy_b['distanceLeftUm'].iloc[2]
            b_southl    = df_sen_mtrlgy_b['distanceLeftUm'].iloc[3]

            b_eastr     = df_sen_mtrlgy_b['distanceRightUm'].iloc[0]
            b_northr    = df_sen_mtrlgy_b['distanceRightUm'].iloc[1]
            b_westr     = df_sen_mtrlgy_b['distanceRightUm'].iloc[2]
            b_southr    = df_sen_mtrlgy_b['distanceRightUm'].iloc[3]

            positions   = ['NORTHL', 'NORTHR', 'EASTL', 'EASTR', 'SOUTHL', 'SOUTHR', 'WESTL', 'WESTR']
            top         = [t_northl, t_northr, t_eastl, t_eastr, t_southl, t_southr, t_westl, t_westr]
            bottom      = [b_northl, b_northr, b_eastl, b_eastr, b_southl, b_southr, b_westl, b_westr]

        else:
            positions   = ['NORTHL', 'NORTHR', 'EASTL', 'EASTR', 'SOUTHL', 'SOUTHR', 'WESTL', 'WESTR']
            top         = [np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan]
            bottom      = [np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan]
            
        dicing_data = {
                        'Dicing Position': positions,
                        'top sensor': top,
                        'bottom sensor': bottom
                    }
        dicing_df = pd.DataFrame(dicing_data)
        return dicing_df
    
    #sort and order metrology information of the module
    def metrology_module(self, data):
        if data:
            df_mod_mtrlgy = pd.DataFrame(data)
            yShift = df_mod_mtrlgy['yShiftUm'][0]
            xShift = df_mod_mtrlgy['xShiftUm'][0]
            mtrlgy_kind = df_mod_mtrlgy['kindOfMetrology'][0]
            rotation = df_mod_mtrlgy['rotationUrad'][0]
        else:
            xShift = np.nan
            yShift = np.nan
            mtrlgy_kind = np.nan
            rotation = np.nan
        
        return mtrlgy_kind, xShift, yShift, rotation
    
    #sort and order iv data of the two sensors and the module and plot the scaled curves
    def iv(self, iv_data, run_type):
        voltage = None
        current = None
        temperature = None
        scaled_current = None

        if iv_data:
            #handle top sensor
            df_iv = pd.DataFrame(iv_data)
            latest_sen = df_iv[df_iv['runType'] == run_type].sort_values(by='runNumber').tail(1)
            if not latest_sen.empty:
                data_iv = df_iv[df_iv['runNumber'] == latest_sen['runNumber'].iloc[0]]
                if 'temp' in data_iv.columns:
                    if data_iv['temp'].isnull().any():
                        pass 
                    else:
                        temperature = data_iv['temp']+273.15
                if data_iv['currntNamp'].isnull().any():
                    pass
                else:
                    current = data_iv['currntNamp']
                    voltage = data_iv['volts']
            else:
                return None, None, None
        else:
            return None, None, None

        scaled_current = self.rescale(current, temperature).reset_index(drop=True)
        return voltage, scaled_current, current
    
    def iv_plot(self, iv_plot_data, title):
        for iv_data in iv_plot_data:
            voltage = iv_data["voltage"]
            current = iv_data["current"]
            color = iv_data["color"]
            label = iv_data["label"]
            plt.plot(voltage.to_numpy(), current.to_numpy(), color=color, label=label)
        plt.xlabel('Voltage (V)')
        plt.ylabel('Leakage Current (nA)')
        plt.legend(loc='lower right')
        plt.title(title)
        plt.tight_layout()
        plt.savefig(f'{self.module_dir}/{title}_iv.jpeg', bbox_inches='tight', dpi=300)
        #plt.show()
        plt.clf()

        return f'{self.module_dir}/{title}_iv.jpeg'
    
    
        if False:
            #modules
            no_iv_mod = 0
            no_bonded_iv = 0
            no_final_iv = 0
            mod_scaling_bonded = 1
            mod_scaling_final = 1
            curr_bonded = curr_final = temp_bonded = temp_final = 0
            scaled_current_bonded = scaled_current_final = None
            if data_mod:
                df_module_iv = pd.DataFrame(data_mod) 
                #print(df_module_iv)
                latest_mod_bonded   = df_module_iv[df_module_iv['runtype'] == 'mod_bonded'].sort_values(by='runnumber').tail(1)
                latest_mod_final    = df_module_iv[df_module_iv['runtype'] == 'mod_final'].sort_values(by='runnumber').tail(1)

                if not latest_mod_bonded.empty:
                    data_mod_bonded = df_module_iv[df_module_iv['runnumber'] == latest_mod_bonded['runnumber'].iloc[0]]
                    if data_mod_bonded['temp'].isnull().any():
                        mod_scaling_bonded = 0
                        raise ValueError("\n###########################################\n Temperature column of bonded module contains NaN values \n###########################################\n")
                    if data_mod_bonded['currntNamp'].isnull().any():
                        mod_scaling_bonded = 0
                        raise ValueError("\n###########################################\n Current column of bonded module contains NaN values \n###########################################\n") 
                    curr_bonded = data_mod_bonded['currntNamp']
                    temp_bonded = data_mod_bonded['temp'] + 273.15
                    volts_bonded= data_mod_bonded['volts'].to_numpy()
                else:
                    print("\n###########################################\n No IV measurement of the bonded module \n###########################################\n")
                    no_bonded_iv = 1
                    mod_scaling_bonded = 0
                    
                if not latest_mod_final.empty:
                    data_mod_final  = df_module_iv[df_module_iv['runnumber'] == latest_mod_final['runnumber'].iloc[0]]
                    if data_mod_final['temp'].isnull().any():
                        mod_scaling_final = 0
                        raise ValueError("\n###########################################\n Temperature column of final module contains NaN values \n###########################################\n")
                    if data_mod_final['currntNamp'].isnull().any():
                        mod_scaling_final = 0
                        raise ValueError("\n###########################################\n Current column of final module contains NaN values \n###########################################\n") 
                    curr_final  = data_mod_final['currntNamp']
                    temp_final  = data_mod_final['temp'] + 273.15
                    volts_final = data_mod_final['volts'].to_numpy()
                else:
                    print("\n###########################################\n No IV measurement of the final module \n###########################################\n")
                    no_final_iv = 1 
                    mod_scaling_final = 0

                if mod_scaling_bonded == 1:
                    scaled_current_bonded   = self.rescale(curr_bonded, temp_bonded)
                if mod_scaling_final == 1:
                    scaled_current_final    = self.rescale(curr_final, temp_final)                
            else: 
                print("\n###########################################\n No IV measurement was taken for the module \n###########################################\n")
                no_iv_mod = 1
        if False:
            #plot that iv curves
                #define some plot parameters
            plt.rc('axes', titlesize=11)     
            plt.rc('axes', labelsize=11)
            plt.rc('xtick', labelsize=11)    
            plt.rc('ytick', labelsize=11)    
            plt.rc('legend', fontsize=11) 


                #iv curves of the two sensors 
            plots = 1
            if no_iv_mod == 0 and no_sen_iv == 0:
                if no_top_iv == 0:
                    plt.plot(data_iv_sen_top['volts'].to_numpy(), data_iv_sen_top['currntNamp'].to_numpy(), color='springgreen', label='top sensor')
                if no_bottom_iv == 0:
                    plt.plot(data_iv_sen_bottom['volts'].to_numpy(), data_iv_sen_bottom['currntNamp'].to_numpy(), color='steelblue', label='bottom sensor')
                plt.xlabel('Voltage (V)')
                plt.ylabel('Leakage Current (nA)')
                plt.legend(loc='lower right')
                plt.title('IV Measurements: 2 Sensors')
                plt.tight_layout()
                #plt.savefig(f'{self.module_dir}/sensor_iv.pdf', bbox_inches='tight', dpi=300)
                plt.savefig(f'{self.module_dir}/sensor_iv.jpeg', bbox_inches='tight', dpi=300)
                #plt.show()
                plt.clf()


                #iv curves of the module after bonding and after encapsulation as well as both sensors added and scaled to 20°C
                if no_bonded_iv == 0:
                    plt.plot(volts_bonded, scaled_current_bonded, color='darkmagenta', label='bonded module')
                if no_final_iv == 0:
                    plt.plot(volts_final, scaled_current_final, color='firebrick', label='encapsulated, final module')
                if no_top_iv == 0 and no_bottom_iv == 0:
                    plt.plot(sum_voltage, scaled_sum_current, color='lightseagreen', label='top + bottom sensor')
                plt.xlabel('Voltage (V)')
                plt.ylabel('Leakage Current (nA)')
                plt.legend(loc='lower right')
                plt.title('IV Measurements: Module and Sum of Sensors Scaled to 20 °C')
                plt.tight_layout()
                #plt.savefig(f'{self.module_dir}/mod_iv.pdf', bbox_inches='tight', dpi=300)
                plt.savefig(f'{self.module_dir}/mod_iv.jpeg', bbox_inches='tight', dpi=300)
                plt.clf()
                #plt.show()
            else:
                plots = 0

        #return plots, no_bonded_iv, no_final_iv, no_top_iv, no_bottom_iv, sen_scaling_top, sen_scaling_bot, mod_scaling_bonded, mod_scaling_final


    #sort and order iv data of the two sensors and the module and plot the scaled curves
    def iv2(self, data_t, data_b, data_mod):
        #sensors
        no_sen_iv = 0
        no_top_iv = 0
        no_bottom_iv = 0
        sen_scaling_top = 1
        sen_scaling_bot = 1
        curr_top = curr_bot = temp_top = temp_bot = 0
        scaled_current_top = scaled_current_bot = scaled_sum_current = None

        if data_t and data_b:
            #handle top sensor
            df_sensor_iv_top = pd.DataFrame(data_t)
            latest_sen_top = df_sensor_iv_top[df_sensor_iv_top['runType'] == 'SQC'].sort_values(by='runNumber').tail(1)
            if not latest_sen_top.empty:
                data_iv_sen_top = df_sensor_iv_top[df_sensor_iv_top['runNumber'] == latest_sen_top['runNumber'].iloc[0]]
                if data_iv_sen_top['temp'].isnull().any():
                    sen_scaling_top = 0
                    raise ValueError("\n###########################################\n Temperature column of top sensor contains NaN values \n###########################################\n")
                if data_iv_sen_top['currntNamp'].isnull().any():
                    sen_scaling_top = 0
                    raise ValueError("\n###########################################\n Current column of top sensor contains NaN values \n###########################################\n")
                curr_top = data_iv_sen_top['currntNamp']
                temp_top = data_iv_sen_top['temp']+273.15
            else:
                print("\n###########################################\n No IV measurement of the top sensor \n###########################################\n")
                no_top_iv = 1
                sen_scaling_top = 0

            #handle bottom sensor
            df_sensor_iv_bottom = pd.DataFrame(data_b) 
            latest_sen_bottom = df_sensor_iv_bottom[df_sensor_iv_bottom['runType'] == 'SQC'].sort_values(by='runNumber').tail(1)
            if not latest_sen_bottom.empty:
                data_iv_sen_bottom = df_sensor_iv_bottom[df_sensor_iv_bottom['runNumber'] == latest_sen_bottom['runNumber'].iloc[0]]
                if data_iv_sen_bottom['temp'].isnull().any():
                    sen_scaling_bot = 0
                    raise ValueError("\n###########################################\n Temperature column of bottom sensor contains NaN values \n###########################################\n")
                if data_iv_sen_bottom['currntNamp'].isnull().any():
                    sen_scaling_bot = 0
                    raise ValueError("\n###########################################\n Current column of bottom sensor contains NaN values \n###########################################\n") 
                curr_bot = data_iv_sen_bottom['currntNamp']
                temp_bot = data_iv_sen_bottom['temp']+273.15   
                sum_voltage = data_iv_sen_bottom['volts']
            else:
                print("\n###########################################\n No IV measurement of the bottom sensor \n###########################################\n")
                no_bottom_iv = 1
                sen_scaling_bot = 0

                #scaling and adding the two sensor currents
            if sen_scaling_top == 1:
                scaled_current_top = self.rescale(curr_top, temp_top).reset_index(drop=True)
            if sen_scaling_bot == 1:
                scaled_current_bot = self.rescale(curr_bot, temp_bot).reset_index(drop=True)
            if no_top_iv == 0 and no_bottom_iv == 0:
                scaled_sum_current = scaled_current_top + scaled_current_bot
        else:
            no_sen_iv = 1
        
        #modules
        no_iv_mod = 0
        no_bonded_iv = 0
        no_final_iv = 0
        mod_scaling_bonded = 1
        mod_scaling_final = 1
        curr_bonded = curr_final = temp_bonded = temp_final = 0
        scaled_current_bonded = scaled_current_final = None
        if data_mod:
            df_module_iv = pd.DataFrame(data_mod) 
            #print(df_module_iv)
            latest_mod_bonded   = df_module_iv[df_module_iv['runtype'] == 'mod_bonded'].sort_values(by='runnumber').tail(1)
            latest_mod_final    = df_module_iv[df_module_iv['runtype'] == 'mod_final'].sort_values(by='runnumber').tail(1)

            if not latest_mod_bonded.empty:
                data_mod_bonded = df_module_iv[df_module_iv['runnumber'] == latest_mod_bonded['runnumber'].iloc[0]]
                if data_mod_bonded['temp'].isnull().any():
                    mod_scaling_bonded = 0
                    raise ValueError("\n###########################################\n Temperature column of bonded module contains NaN values \n###########################################\n")
                if data_mod_bonded['currntNamp'].isnull().any():
                    mod_scaling_bonded = 0
                    raise ValueError("\n###########################################\n Current column of bonded module contains NaN values \n###########################################\n") 
                curr_bonded = data_mod_bonded['currntNamp']
                temp_bonded = data_mod_bonded['temp'] + 273.15
                volts_bonded= data_mod_bonded['volts'].to_numpy()
            else:
                print("\n###########################################\n No IV measurement of the bonded module \n###########################################\n")
                no_bonded_iv = 1
                mod_scaling_bonded = 0
                
            if not latest_mod_final.empty:
                data_mod_final  = df_module_iv[df_module_iv['runnumber'] == latest_mod_final['runnumber'].iloc[0]]
                if data_mod_final['temp'].isnull().any():
                    mod_scaling_final = 0
                    raise ValueError("\n###########################################\n Temperature column of final module contains NaN values \n###########################################\n")
                if data_mod_final['currntNamp'].isnull().any():
                    mod_scaling_final = 0
                    raise ValueError("\n###########################################\n Current column of final module contains NaN values \n###########################################\n") 
                curr_final  = data_mod_final['currntNamp']
                temp_final  = data_mod_final['temp'] + 273.15
                volts_final = data_mod_final['volts'].to_numpy()
            else:
                print("\n###########################################\n No IV measurement of the final module \n###########################################\n")
                no_final_iv = 1 
                mod_scaling_final = 0

            if mod_scaling_bonded == 1:
                scaled_current_bonded   = self.rescale(curr_bonded, temp_bonded)
            if mod_scaling_final == 1:
                scaled_current_final    = self.rescale(curr_final, temp_final)                
        else: 
            print("\n###########################################\n No IV measurement was taken for the module \n###########################################\n")
            no_iv_mod = 1
        
        #plot that iv curves
            #define some plot parameters
        plt.rc('axes', titlesize=11)     
        plt.rc('axes', labelsize=11)
        plt.rc('xtick', labelsize=11)    
        plt.rc('ytick', labelsize=11)    
        plt.rc('legend', fontsize=11) 


            #iv curves of the two sensors 
        plots = 1
        if no_iv_mod == 0 and no_sen_iv == 0:
            if no_top_iv == 0:
                plt.plot(data_iv_sen_top['volts'].to_numpy(), data_iv_sen_top['currntNamp'].to_numpy(), color='springgreen', label='top sensor')
            if no_bottom_iv == 0:
                plt.plot(data_iv_sen_bottom['volts'].to_numpy(), data_iv_sen_bottom['currntNamp'].to_numpy(), color='steelblue', label='bottom sensor')
            plt.xlabel('Voltage (V)')
            plt.ylabel('Leakage Current (nA)')
            plt.legend(loc='lower right')
            plt.title('IV Measurements: 2 Sensors')
            plt.tight_layout()
            #plt.savefig(f'{self.module_dir}/sensor_iv.pdf', bbox_inches='tight', dpi=300)
            plt.savefig(f'{self.module_dir}/sensor_iv.jpeg', bbox_inches='tight', dpi=300)
            #plt.show()
            plt.clf()


            #iv curves of the module after bonding and after encapsulation as well as both sensors added and scaled to 20°C
            if no_bonded_iv == 0:
                plt.plot(volts_bonded, scaled_current_bonded, color='darkmagenta', label='bonded module')
            if no_final_iv == 0:
                plt.plot(volts_final, scaled_current_final, color='firebrick', label='encapsulated, final module')
            if no_top_iv == 0 and no_bottom_iv == 0:
                plt.plot(sum_voltage, scaled_sum_current, color='lightseagreen', label='top + bottom sensor')
            plt.xlabel('Voltage (V)')
            plt.ylabel('Leakage Current (nA)')
            plt.legend(loc='lower right')
            plt.title('IV Measurements: Module and Sum of Sensors Scaled to 20 °C')
            plt.tight_layout()
            #plt.savefig(f'{self.module_dir}/mod_iv.pdf', bbox_inches='tight', dpi=300)
            plt.savefig(f'{self.module_dir}/mod_iv.jpeg', bbox_inches='tight', dpi=300)
            plt.clf()
            #plt.show()
        else:
            plots = 0

        return plots, no_bonded_iv, no_final_iv, no_top_iv, no_bottom_iv, sen_scaling_top, sen_scaling_bot, mod_scaling_bonded, mod_scaling_final
 
    #sort and order the pulltest data
    def pulltest(self, data):
        if data:
            df_pulltest = pd.DataFrame(data) 

            all_pullLoc = ['Bottom Right', 'Bottom Left', 'Top Right', 'Top Left']
            existing_pullLoc = df_pulltest['pullLoc'].unique()
            missing_pullLoc = set(all_pullLoc) - set(existing_pullLoc)
            missing_data = pd.DataFrame({
                'pullLoc': list(missing_pullLoc),
                'forceHybrid': [np.nan] * len(missing_pullLoc),
                'forceSensor': [np.nan] * len(missing_pullLoc)
            })
            df_pulltest = pd.concat([df_pulltest, missing_data], ignore_index=True)

            hybrid = sen = err_hybrid = err_sen = 0 
            pulltest = None

            sen = df_pulltest.groupby('pullLoc')['forceSensor'].mean().reset_index()
            sen.columns = ['pullLoc', 'mean_forceSensor']

            hybrid= df_pulltest.groupby('pullLoc')['forceHybrid'].mean().reset_index()
            hybrid.columns = ['pullLoc', 'mean_forceHybrid']
            sen['rmse_Sensor'] = np.sqrt(df_pulltest.groupby('pullLoc')['forceSensor'].apply(lambda x: ((x - x.mean()) ** 2).mean()).values)
            hybrid['rmse_Hybrid'] = np.sqrt(df_pulltest.groupby('pullLoc')['forceHybrid'].apply(lambda x: ((x - x.mean()) ** 2).mean()).values)

            pulltest = pd.merge(sen, hybrid, on='pullLoc')
        else:
            df_pulltest =   {
                                'pullLoc': ['Top Left', 'Top Right', 'Bottom Left', 'Bottom Right'],
                                'mean_forceSensor': [np.nan, np.nan, np.nan, np.nan],
                                'mean_forceHybrid': [np.nan, np.nan, np.nan, np.nan],
                                'rmse_Sensor': [np.nan, np.nan, np.nan, np.nan],
                                'rmse_Hybrid': [np.nan, np.nan, np.nan, np.nan],
                            }
            pulltest = pd.DataFrame(df_pulltest)
        
        return pulltest

#additional functions needed for the main functions and the pdf layout
    #generate qr code for the module and each component
    def generate_qr_code(self, data, file_path):
        qr = qrcode.QRCode(version=1, box_size=10, border=4)
        qr.add_data(data)
        qr.make(fit=True)
        qr_img = qr.make_image(fill='black', back_color='white')
        qr_img.save(file_path)
        return qr_img
    
    #rescale the currents to 20°C
    def rescale(self, curr, temp):
        if temp is not None:
            E_eff = 1.21    #effective energy band gap
            k_B = 8.6e-5    #boltzman constant
            T_s = 293.15    #temperature to scale the values to (20°C)
            I_scaled = (curr/(temp**2*np.exp(-E_eff/(2*k_B*temp)))*(T_s**2*np.exp(-E_eff/(2*k_B*T_s))))
            return I_scaled
        else:
            return curr    
    #color the dicing values
    def get_color_dicing(self, value):
        value = float(value)
        if 235 <= value <= 245:
            return colors.green
        elif 230 <= value < 235 or 245 < value <= 250:
            return colors.orange
        else:
            return colors.red 
        
    #color the module metrology values
    def shift_col(self, value):
        value = float(np.abs(value))
        if value <= 50:
            return colors.green
        else:
            return colors.red 

    #color rotation values
    def rot_col(self, value):
        value = float(np.abs(value))
        if value <= 400:
            return colors.green
        else:
            return colors.red  
        
    #color mean pulltest values
    def pulltest_mean_color(self, value):
        value = float(value)
        if value > 8:
            return colors.green
        else:
            return colors.red 
    
    #color error pulltest values
    def pulltest_rmse_color(self, value, mean_value):
        value = float(value)
        if value < (mean_value/10):
            return colors.green
        else:
            return colors.red 

    #format pulltest values
    def format_value(self, value, color=None):
        if pd.notnull(value):
            value = float(value)
            formatted_value = f"{value:.1f}".rstrip('0').rstrip('.')
            if color:
                return f'<font color="{color}">{formatted_value}</font>'
            else:
                return formatted_value
        else:
            return f'<font color="{colors.red}">NaN</font>'

    #reformat pulltest values
    def get_formatted_value(self, pulltest, row, type):
        mean_value = pulltest.at[row, f'mean_force{type}']
        rmse_value = pulltest.at[row, f'rmse_{type}']
        mean_color = self.pulltest_mean_color(mean_value) if pd.notnull(mean_value) else colors.red
        rmse_color = self.pulltest_rmse_color(rmse_value, mean_value) if pd.notnull(rmse_value) else colors.red
        
        formatted_mean = self.format_value(mean_value, mean_color)
        formatted_rmse = self.format_value(rmse_value, rmse_color)
        
        return f"{formatted_mean} \u00B1 {formatted_rmse}" if pd.notnull(mean_value) else self.format_value('NaN', colors.red)
