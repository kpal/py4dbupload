#!/usr/bin/python
# $Id$
# Created by Alessandro Di Mattia <dimattia@cern.ch>, 06-Feb-2025
# This module contains the classes that produce the XML file for uploading
# the bare module, the module and to set the links with the sub-components.

from AnsiColor    import Fore, Style
from Exceptions   import NotRecognized,MissingDataInDB,BadParameters,IntegrityError
from BaseUploader import BaseUploader,ComponentType
from Utils        import IdType
from lxml         import etree
from copy         import deepcopy 

import sys


IT_bareMod_vendors = {'ADV'  :'Advafab',
                      'MIC'  :'Micross',
                      'IZM'  :'IZM'}

class ITBareModule(BaseUploader):
   """Class to produce the xml file for the DBloader. It handles the IT Bare
      module component data read from the csv file shipped by vendors."""

   def __init__(self, module_name, fabric_date, rework_date, cus_remarks, location=None):
      """Constructor: requires the module name (module_name), the production 
         date (fabric_date), the possible reworking date (rework_date), the 
         comments and remark from the vendor (cus_remark) and the location of
         the component (location).
         Mandatory parameters for the object configuration:
            manufacturer:    the manufacturer of the Wafer;
            attributes:      the additional attributes for the Wafer;
            kind_of_part:    the part to be inserted in the database;
            unique_location: the location where the component is located;
      """

      self.type = ComponentType.ITBareModule
      #cdict = deepcopy(configuration)
      cdict = {}
      
      self.sensor = ''
      self.crocs  = {'0':(), '1':(), '2':(), '3':()}
      
      self.sensor_id = 0
      self.crocs_id  = [0,0,0,0]
      
      # Get the manifacturer from the component name
      try:
         manufacturer_tag = module_name.split('-')[2]
         manufacturer = IT_bareMod_vendors[manufacturer_tag]
      except IndexError:
         print(f'Cannot get a valid manufacturer tag from the Bare Module name {module_name}')
         sys.exit(1)
      cdict['manufacturer'] = manufacturer
      
      # Set the component location
      if location!=None and location!='': cdict['unique_location'] = location
      else: cdict['unique_location'] = manufacturer
            
      # Set the product date
      cdict['product_date'] = fabric_date

      # Set kind_of_part
      cdict['kind_of_part'] = self.kind_of_part(module_name)
              
      # Attach attributes
      if rework_date!='':
         cdict['attributes'] = [('Reworked','Yes')]
      else:
         cdict['attributes'] = [('Reworked','No')]
                  
      # Set the comment if present            
      if cus_remarks!='': cdict['description'] = cus_remarks

      BaseUploader.__init__(self, module_name, cdict)
      self.mandatory += ['kind_of_part','unique_location','manufacturer',\
                          'attributes']

   def add_sensor(self,name):
      """Adds an IT sensor to be attached as subcomponent."""
      try:
          # For 3D the sensitive field is the second one, while for the other is the third one
          pos_tag = name.split('_')[1] if name.split('_')[1]=='3D' else name.split('_')[2]
          if   'quad' in pos_tag:   self.sensor = f'HPK_quad_{name}'
          elif 'double' in pos_tag: self.sensor = f'HPK_double_{name}'
          elif '3D' in pos_tag: self.sensor = name 
          else: raise Exception()
      except:
          raise ValueError(f'Unrecognised sensor name: "{name}"!')
      
      # Check if sensor is registered in the database
      sensor_id = self.db.component_id(self.sensor, idt='name_label', kop=self.sensor_kind_of_part())
      if (sensor_id==None):
         sensor = self.sensor
         self.sensor = ''
         raise MissingDataInDB(self.__class__.__name__,f'Sensor {sensor} not registered in the database')
      self.sensor_id = sensor_id
      
      # Check if sensor has the same location of the bare module
      location = self.retrieve('unique_location')
      m_loc_id = self.db.get_location_id(location)
      s_loc_id = self.db.component_location(self.sensor, idt='name_label')
      if m_loc_id!=s_loc_id:
         sensor = self.sensor
         self.sensor = ''
         raise IntegrityError(self.__class__.__name__,f'Mismatch among module and sensor location: sensor {sensor}, module location {location}')
           
       
   def sensor_kind_of_part(self,name=None):
      """Returns the sensor subcomponent kind of part."""
      if name==None:  name = self.sensor
      try:
         pos_tag = self.sensor.split('_')[4]
         if   'quad'   in pos_tag: return "IT Quad Sensor"
         elif 'double' in pos_tag: return "IT Double Sensor"
         elif ','      in pos_tag: return "IT 3D Sensor"
         else: raise Exception()
      except:
         raise ValueError("Either the sensor is not attached or its name is not recognized.")
           
       
   def add_croc(self,wafer,position,slot,fp_ok):
      """Adds a CROC to be attached as subcomponent."""
      wafer_name = wafer.replace('-','_')
      croc_name  = f'{wafer_name}_{position}'
      if int(slot) <0 or int(slot)>3:
         raise BadParameters(self.__class__.__name__,"CROC Slot must be between 0 and 3")
  
      # Check if CROC is registered in the database, we suppose CROCs are all "CROC Chip" kinds
      chip_id = self.db.component_id(croc_name, idt='name_label', kop='CROC Chip')
      if (chip_id==None):
         self.crocs[slot]=()
         raise MissingDataInDB(self.__class__.__name__,f'CROC {croc_name} not registered in the database')
      
      # Check if CROC has the same location of the bare module
      location = self.retrieve('unique_location')
      m_loc_id = self.db.get_location_id(location)
      s_loc_id = self.db.component_location(croc_name, idt='name_label')
      if m_loc_id!=s_loc_id:
         raise IntegrityError(self.__class__.__name__,f'Mismatch among module and CROC location: CROC {croc_name}, module location {location}')

      self.crocs[slot]=(chip_id,croc_name,fp_ok)


   def module_type(self,name=None):
      """Returns the module type (quad,double,3D) from its name."""
      if name==None: name=self.name

      try:
         tag = name.split('-')[1]
      except IndexError:
         print(f'Cannot get a valid type tag from the Bare Module name {name}')
         sys.exit(1)
    
      if   tag=='Q' :   return "quad"
      elif tag=='D' :   return "double"
      elif tag=='S' :   return "3d"
      else : raise NotRecognized(self.__class__.__name__,tag,' as a valid tag type for an IT Bare Module.')

   def number_of_crocs(self,name=None):
      """Return the number of CROC associated to this module."""
      if name==None: name=self.name
      typ = self.module_type(name)
      
      if   typ=='quad'  : return 4
      elif typ=='double': return 2
      elif typ=='3d'    : return 1

   def crocs_loaded(self):
      """Return the number of CROC loaded."""
      return len([croc for croc in self.crocs.values() if croc!=()])

   def barcode(self):
      """Returns the barcode as the name of the component."""
      return self.name_label()
   
   def name_label(self):
      """Returns the name label of the component."""
      return self.name

   def kind_of_part(self, name=None):
      """Return the kind_of_part to be associated to this module type."""
      if name==None: name=self.name
      typ = self.module_type(name)
      
      if   typ=='quad'   :   return "IT Quad Bare Module"
      elif typ=='double' :   return "IT Double Bare Module"
      elif typ=='3d'     :   return "IT 3D Bare Module"

   def subitems(self):
      """Returns the subcomponents connected to this module."""
      sub = []
      
      if self.sensor!='':
         s_conf = {'kind_of_part':self.sensor_kind_of_part()}
         sub.append( ITBareSubcomponent(self.sensor_id,self.sensor,s_conf,self.name) )
      for slot in range(self.crocs_loaded()):
         croc = self.crocs[str(slot)]
         b_status = 'Good' if croc[2]=='1' else 'Bad'
         c_conf = {'kind_of_part': 'CROC Chip',
                   'attributes': [('CROC Slot',f'{slot}'),('CROC Bonding Status',f'{b_status}')] }
         sub.append( ITBareSubcomponent(croc[0],croc[1],c_conf,self.name) )

      return sub
      
   
   def match(self,name):
      """Return True if the input barcode matches the class name."""
      if self.name==name: return True
      return False

   def dump_xml_data(self, filename=''):
      """Writes the wafer components in the XML file for the database upload.
         It requires in input the name of the XML file to be produced."""
      inserter  = self.retrieve('inserter')
      root      = etree.Element("ROOT")
      if inserter is not None:
         root.set('operator_name',inserter)
         
      parts = etree.SubElement(root,"PARTS")

      self.xml_builder(parts)

      if filename == '':
         filename = '{}_wafer.xml'.format(self.name)

      with open(filename, "w") as f:
         f.write( etree.tostring(root, pretty_print = True, \
                                       xml_declaration = True,\
                                       standalone="yes").decode('UTF-8') )


   def xml_builder(self, parts, *args):
      """Processes data in the reader."""
      label   = self.name_label()

      # No extended data for the IT Bare Module
      croc_fp=[]
      #print (self.crocs)
      for slot in self.crocs.keys():
         if len(self.crocs[slot])!=0: croc_fp.append(self.crocs[slot][2])
      
      croc_status = 'Good' if all(fp_status == '1' for fp_status in croc_fp) else 'Bad'
      #print (croc_status)
      attributes = self.retrieve('attributes')
      attributes.append( ('Status', croc_status) )
      self.update_configuration('attributes',attributes)

      part_id = self.db.component_id(label,'name_label',self.kind_of_part())
      if part_id==None:
         # not registered
         self.build_parts_on_xml(parts,name_label=label)




class ITBareSubcomponent(BaseUploader):
   """Class to produce the xml file for updating the Module sub components."""

   def __init__(self, part_id, name, configuration, module_name):
      """Constructor: requires the component id (part_id), the name of the
         component (name), the dicionary for configuration (configuration)
         where the parameter to be updated are defined and the module name
         (module_name) which this component have to be associated with.
      """

      cdict = deepcopy(configuration)
      self.part_id = part_id
      self.module_name = module_name
      
      BaseUploader.__init__(self, name, cdict)
      self.mandatory += ['kind_of_part']

   def match(self,name):
      """Return True if the input barcode matches the class name."""
      if self.name==name: return True
      return False

   def dump_xml_data(self, filename=''):
      """Writes the wafer components in the XML file for the database upload.
         It requires in input the name of the XML file to be produced."""
      inserter  = self.retrieve('inserter')
      root      = etree.Element("ROOT")
      if inserter is not None:
         root.set('operator_name',inserter)
         
      parts = etree.SubElement(root,"PARTS")

      self.xml_builder(parts)

      if filename == '':
         filename = '{}_wafer.xml'.format(self.name)

      with open(filename, "w") as f:
         f.write( etree.tostring(root, pretty_print = True, \
                                       xml_declaration = True,\
                                       standalone="yes").decode('UTF-8') )


   def xml_builder(self, parts, *args):
      """Processes data in the reader."""
      modules = args[0]

      children = parts
      if modules is not None:
         for module in modules:
            if module.name==self.module_name:
               module_kop  = module.kind_of_part()
               module_part = etree.SubElement(parts, "PART", mode="auto")
               etree.SubElement(module_part,"KIND_OF_PART").text  = module_kop
               etree.SubElement(module_part,"NAME_LABEL").text = module.name
               children = etree.SubElement(module_part,"CHILDREN")
      
      # Update the component
      self.update_parts_on_xml(children,self.part_id)