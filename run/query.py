from Query import Query
from optparse     import OptionParser
import json

if __name__ == "__main__":
    p = OptionParser(usage="usage: %prog [options]", version="1.0")

    p.add_option( '--dev',
                    action  = 'store_true',
                    default = False,
                    dest    = 'isDevelopment',
                    help    = 'Set the development database as target.')

    p.add_option( '--verbose',
                    action  = 'store_true',
                    default = False,
                    dest    = 'verbose',
                    help    = 'Force the uploaders to print their configuration and data.')

    p.add_option( '--locations',
                    action  = 'store_true',
                    default = False,
                    dest    = 'locations',
                    help    = 'Print all locations stored in the DB')
    
    p.add_option( '--testConditions2s',
                    action  = 'store_true',
                    default = False,
                    dest    = 'testConditions2s',
                    help    = 'Get table of test conditions for 2S modules')

    p.add_option( '--testConditionsPs',
                    action  = 'store_true',
                    default = False,
                    dest    = 'testConditionsPs',
                    help    = 'Get table of test conditions for PS modules')
    
    p.add_option( '--testConditionsSkeleton',
                    action  = 'store_true',
                    default = False,
                    dest    = 'testConditionsSkeleton',
                    help    = 'Get table of test conditions for 2S skeletons')

    p.add_option( '--giphtTestConditions',
                    action  = 'store_true',
                    default = False,
                    dest    = 'giphtTestConditions',
                    help    = 'Get table of test conditions for gipht')

    p.add_option( '--otModuleInformation',
                    type    = 'string',
                    default = False,
                    dest    = 'otModuleInformation',
                    metavar = 'STR',
                    help    = 'Get OT module information')

    p.add_option( '--metrology',
                    type    = 'string',
                    default = None,
                    dest    = 'metrology',
                    metavar = 'STR',
                    help    = 'Get metrology results of a module')
    
    p.add_option( '--sensorMetrology',
                    type    = 'string',
                    default = None,
                    dest    = 'sensor_metrology',
                    metavar = 'STR',
                    help    = 'Get metrology results of a sensor')

    p.add_option( '--moduleIV',
                    type    = 'string',
                    default = None,
                    dest    = 'moduleIV',
                    metavar = 'STR',
                    help    = 'Get IV results of a module')
    p.add_option( '--moduleRootFileList',
                    type    = 'string',
                    default = None,
                    dest    = 'otModuleRootFile',
                    metavar = 'STR',
                    help    = 'Get List of root file entries of a module')

    p.add_option( '-t','--test',
                    action  = 'store_true',
                    default = False,
                    dest    = 'test',
                    help    = 'Test')

    p.add_option( '--sensorIV',
                    type    = 'string',
                    default = None,
                    dest    = 'sensorIV',
                    metavar = 'STR',
                    help    = 'Get IV results of a sensor')
    
    p.add_option( '--2SModules',
                    type    = 'string',
                    default = None,
                    dest    = 'ot2SManufacturer',
                    metavar = 'STR',
                    help    = 'Get 2S Modules produced by a manufacturer')

    p.add_option( '--PSModules',
                    type    = 'string',
                    default = None,
                    dest    = 'otPSManufacturer',
                    metavar = 'STR',
                    help    = 'Get PS Modules produced by a manufacturer')

    p.add_option( '--boxes',
                    action  = 'store_true',
                    default = False,
                    dest    = 'boxes',
                    help    = 'Get location of boxes')


    p.add_option( '-l','--last',
                    action  = 'store_true',
                    default = False,
                    dest    = 'last',
                    help    = 'Only get last entry')

    p.add_option( '-p','--pipe',
                    type    = 'string',
                    default = None,
                    dest    = 'pipe',
                    metavar = 'STR',
                    help    = 'Named pipe to forward data to')

    p.add_option( '--type',
                    type    = 'string',
                    default = 'All',
                    dest    = 'prodType',
                    help    = 'Only get last entry')

    p.add_option( '--2fa',
                action  = 'store_true',
                default = False,
                dest    = 'twofa',
                help    = 'Set the two factor authentication login.')

    (opt, args) = p.parse_args()

    db = 'trker_cmsr'
    if opt.isDevelopment:
        db = 'trker_int2r'
        
    query = Query(db,'login' if not opt.twofa else 'login2', opt.verbose)

    if opt.pipe:
        named_pipe = opt.pipe
    query_valid = True
    try:

        data = None
        if opt.locations:
            data = query.get_locations()

        if opt.metrology:
            data = query.get_ot_module_metrology(opt.metrology,opt.last)
        
        if opt.sensor_metrology:
            data = query.get_ot_sensor_metrology(opt.sensor_metrology,opt.last)

        if opt.moduleIV:
            data = query.get_ot_module_iv(opt.moduleIV,opt.last)

        if opt.sensorIV:
            data = query.get_ot_sensor_iv(opt.sensorIV,opt.last)

        if opt.testConditions2s:
            data = query.get_ot_test_conditions_2s(opt.last)

        if opt.testConditionsPs:
            data = query.get_ot_test_conditions_ps(opt.last)

        if opt.testConditionsSkeleton:
            data = query.get_ot_test_conditions_skeleton(opt.last)

        if opt.giphtTestConditions:
            data = query.get_gipht_test_conditions()

        if opt.otPSManufacturer:
            data = query.get_ot_ps_module_by_manufacturer(opt.otPSManufacturer,opt.prodType)

        if opt.ot2SManufacturer:
            data = query.get_ot_2s_module_by_manufacturer(opt.ot2SManufacturer,opt.prodType)

        if opt.otModuleInformation:
            if "2S" in opt.otModuleInformation:
                data = query.get_ot_module_information_2s(opt.otModuleInformation)
            elif "PS" in opt.otModuleInformation:
                data = query.get_ot_module_information_ps(opt.otModuleInformation)
        if opt.otModuleRootFile:
            data = query.get_ot_module_root_file_list(opt.otModuleRootFile, opt.last)
        if opt.boxes:
            data = query.get_ot_boxes()
        if opt.test:
            data = query.test()
    except Exception as e:
        print(e)
        print("Query not possible")
        query_valid = False


    if opt.pipe:
        with open(named_pipe, "w") as pipe:
            if query_valid:
                pipe.write(json.dumps(data))
            else:
                pipe.write("")
    else:
        if query_valid:
            [print(dataPoint) for dataPoint in data]
