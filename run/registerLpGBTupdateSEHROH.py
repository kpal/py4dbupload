#!/usr/bin/env python
# $Id$
# Created by Stefan Maier  <stefan.maier@cern.ch>, 07.11.24

# This script to update lpgbt bandwidth based on serial number of rohs

import os,time,datetime, csv

from AnsiColor    import Fore, Back, Style
from DataReader   import TableReader
from Hybrid       import ROHSEHlpGBTUpdate
from Query        import Query
from BaseUploader import BaseUploader
from Utils        import UploaderContainer,DBupload,DBaccess,search_files
from optparse     import OptionParser
from progressbar  import *


if __name__ == "__main__":
   p = OptionParser(usage="usage: %prog [options] ", version="1.1")

   p.add_option( '--2fa',
               action  = 'store_true',
               default = False,
               dest    = 'twofa',
               help    = 'Set the two factor authentication login.')

   p.add_option( '--verbose',
               action  = 'store_true',
               default = False,
               dest    = 'verbose',
               help    = 'Force the uploaders to print their configuration and data')
   
   p.add_option( '--debug',
               action  = 'store_true',
               default = False,
               dest    = 'debug',
               help    = 'Force the verbose options in the network query uploaders to print their configuration and data')
   
   p.add_option( '--dev',
               action  = 'store_true',
               default = False,
               dest    = 'isDevelopment',
               help    = 'Set the development database as target.')
   
   p.add_option( '--upload',
               action  = 'store_true',
               default = False,
               dest    = 'upload',
               help    = 'Perform the data upload in database')

   p.add_option( '-y','--hybrid',
               type    = 'string',
               default = '',
               dest    = 'hybrid',
               metavar = 'STR',
               help    = 'Serial Number of the SEH or ROH')

   p.add_option( '-l','--lpgbt',
               type    = 'string',
               default = '',
               dest    = 'lpgbt',
               metavar = 'STR',
               help    = 'ID of the lpGBT chip on the SEH or ROH')

   (opt, args) = p.parse_args()

   db = 'trker_cmsr'
   if opt.isDevelopment:
      db = 'trker_int2r'

   BaseUploader.database = 'cmsr' if opt.isDevelopment==False else 'int2r'
   BaseUploader.verbose  = opt.verbose
   BaseUploader.debug    = opt.debug
   BaseUploader.login = 'login' if not opt.twofa else 'login2'

   query = Query(db,'login' if not opt.twofa else 'login2', opt.verbose)
   if "2SSEH" in opt.hybrid:
      infos = query.get_seh_info(opt.hybrid)
   elif "PSROH" in opt.hybrid:
      infos = query.get_roh_info(opt.hybrid)
   else:
      print("According to ID, part given ({}) is not a SEH or ROH!, Abort attachement.".format(opt.hybrid))
      exit(0)

   print(infos)
   # Some Sanity checks
   if len(infos) == 0:
      print("Query for Hybrid {} not possible. Abort attachement.".format(opt.hybrid))
      exit(0)
   else:
      info = infos[0]

   if info["kindOfPart"] not in ["2S Service Hybrid", "PS Read-out Hybrid"]:
      print("Part given ({}) is not a SEH or ROH!, Abort attachement.".format(opt.hybrid))

   if "childId" in info.keys():
      print("Hybrid {} already has a child component with the ID {}. Abort attachement.".format(opt.hybrid, info["childId"]))
      exit(0)
   else:
      print("No Children attached to {0}, create XML to create and attach lpGBT {1} to {0}".format(opt.hybrid, opt.lpgbt))

   #Get location list
   locations=query.get_locations()
   loc = [location for location in locations if location["locationId"] == info["locationId"]]   [0]["locationName"]

   print(info, "\n" , loc)

   rohseh_update = UploaderContainer('ROHSEHlpGBTUpdate')
   if "2SSEH" in opt.hybrid:
      part = ROHSEHlpGBTUpdate({'kind_of_part':'2S Service Hybrid','part_id': info['id'], 'lpgbt' : {'serial_number': opt.lpgbt, 'location': loc} } )
   elif "PSROH" in opt.hybrid:
      part = ROHSEHlpGBTUpdate({'kind_of_part':'PS Read-out Hybrid','part_id': info['id'], 'lpgbt' : {'serial_number': opt.lpgbt, 'location': loc} } )

   rohseh_update.add(part)
   file = rohseh_update.dump_xml_data()

   path = os.path.dirname(os.environ.get('DBLOADER'))
   db_loader = DBupload(database=BaseUploader.database,path_to_dbloader_api=path,\
                        login_type=BaseUploader.login,verbose=True)

   if opt.upload:
      print("Upload correction")
      db_loader.upload_data(file)

   if not opt.store:
      print("Remove temporary files")
      os.remove(file)
