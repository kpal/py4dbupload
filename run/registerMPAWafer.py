#!/usr/bin/env python
# $Id$
# Created by Alessandro Di Mattia <dimattia@cern.ch>, 29-Dic-2022

# This script masters the registration of the MPA Chips and Wafers components.


import os,traceback,sys,time
#import numpy as np
#import pandas as pd

from DataReader   import TableReader
from MPA          import MPAwafer,MPAchip, MPAWaferTest
from Utils        import search_files, UploaderContainer, DBupload
from BaseUploader import BaseUploader
from datetime     import date
from dateutil     import parser
from optparse     import OptionParser
#from decimal      import Decimal, ROUND_HALF_UP
from progressbar  import *



if __name__ == "__main__":
   p = OptionParser(usage="usage: %prog [options] [xlsx table file]", version="1.1")

   p.add_option( '-d','--data',
               type    = 'string',
               default = '',
               dest    = 'data_path',
               metavar = 'STR',
               help    = 'Path to the csv tables with SSA wafer data.')

   p.add_option( '--lot',
               type    = 'string',
               default = '',
               dest    = 'lot',
               metavar = 'STR',
               help    = 'Lot Number to be uploaded from file')

   p.add_option( '--date',
               type    = 'string',
               default = '',
               dest    = 'date',
               metavar = 'STR',
               help    = 'Date of the component productioni; overwitten by the date in the cvs table.')

   p.add_option( '-v','--ver',
               type    = 'string',
               default = '1.0',
               dest    = 'ver',
               metavar = 'STR',
               help    = 'Version type of the components; overwitten by the tag in the csv table.')

   p.add_option( '-o','--operator',
               type    = 'string',
               default = '',
               dest    = 'operator',
               metavar = 'STR',
               help    = 'The operator that performed the data registration')

   p.add_option( '--dev',
                  action  = 'store_true',
                  default = False,
                  dest    = 'isDevelopment',
                  help    = 'Set the development database as target.')

   p.add_option( '--includeTest',
                  action  = 'store_true',
                  default = False,
                  dest    = 'includeTest',
                  help    = 'Include the upload of condition data')
   
   p.add_option( '--2fa',
               action  = 'store_true',
               default = False,
               dest    = 'twofa',
               help    = 'Set the two factor authentication login.')

   p.add_option( '--upload',
               action  = 'store_true',
               default = False,
               dest    = 'upload',
               help    = 'Perform the upload of the XML file.')

   p.add_option( '--verbose',
               action  = 'store_true',
               default = False,
               dest    = 'verbose',
               help    = 'Force the uploaders to print their configuration and data')

   p.add_option( '--debug',
               action  = 'store_true',
               default = False,
               dest    = 'debug',
               help    = 'Force the verbose options in the network query uploaders to print their configuration and data')


   (opt, args) = p.parse_args()


   if len(args)>1:
      p.error("accepts at most 1 argument!")


   BaseUploader.verbose = opt.verbose
   BaseUploader.debug = opt.debug
   BaseUploader.login = 'login' if not opt.twofa else 'login2'
   
   # Getting the part data from database
   BaseUploader.database = 'cmsr' if opt.isDevelopment==False else 'int2r'

   wafer_file = opt.data_path   
   wafer_file_reader   = TableReader(wafer_file, 'MPA LOTS', d_offset=0, t_offset=1, tabSize=23)
   print ('\nWafer file content',   wafer_file_reader)
   wafer_file_content= wafer_file_reader.getDataAsCWiseDictRowSplit()

   lot_information = {}
   #get information about lot to be uploaded
   for line in wafer_file_content:
      if line['Lot N'] == opt.lot:
         lot_information = line


   wafers = UploaderContainer('MPAwafers')
   wafer_test_files = []
   for i in range(1,int(lot_information["Wafer N"]) +  1):
      wafer_name = lot_information["Lot ID"] + "_" + f"{i:02}" 

      data = {
         'lot_name'  : lot_information["Lot ID"],
         'wafer'     : f"{i:02}",
         'date'      : lot_information["Production date"]
      }
      wafer_conf = {
         'version'          : opt.ver,
         'product_date'     : lot_information["Production date"],
      }
   
      wafer = MPAwafer(wafer_conf,data)
      wafers.add(wafer)
      if opt.includeTest:
         wafer_tests = UploaderContainer('MPAwaferTests'+str(i))
         wafer_test = MPAWaferTest(wafer, lot_information)

         wafer_tests.add(wafer_test)
         wafer_test_files.append(wafer_tests.dump_xml_data(pSkipPartsBlock = True))

      time.sleep(0.1)

   print('\n')
   streams.flush()
   file_to_be_registered = wafers.dump_xml_data()

   path = os.path.dirname(os.environ.get('DBLOADER'))
   db_loader = DBupload(database=BaseUploader.database,path_to_dbloader_api=path,\
                        login_type=BaseUploader.login,verbose=True)
   if opt.upload:  
      db_loader.upload_data(file_to_be_registered)

      if opt.includeTest:
         print("Condition test can be uploaded")
         if opt.upload:
            for file in wafer_test_files:
               db_loader.upload_data(file)

