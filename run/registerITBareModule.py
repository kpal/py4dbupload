#!/usr/bin/env python
# $Id$
# Created by Alessandro Di Mattia <dimattia@cern.ch>, 31-January-2025

# This script masters the registration of the IT Bare module components

import os,traceback,sys,time

from AnsiColor    import Fore, Back, Style
from DataReader   import TableReader,scale,scale2
from ITModule     import ITBareModule
from BaseUploader import BaseUploader
from Utils        import UploaderContainer,DBupload,DBaccess,search_files
from datetime     import date,datetime
from dateutil     import parser
from optparse     import OptionParser

from progressbar  import *


def Upload(filename):
   import sys
   path = os.path.dirname(os.environ.get('DBLOADER'))
   db_loader = DBupload(database=BaseUploader.database,path_to_dbloader_api=path,\
                      login_type=BaseUploader.login,verbose=opt.debug)
   sys.stdout.write(f'Uploading {filename} .... ')
   t = datetime.now()
   sys.stdout.flush()
   db_loader.upload_data(filename)
   elapsed_time = (datetime.now() - t).total_seconds()
   print(f'terminated in {elapsed_time} seconds')

if __name__ == "__main__":
   p = OptionParser(usage="usage: %prog [options] ", version="1.1")

   p.add_option( '-d','--data',
               type    = 'string',
               default = '',
               dest    = 'data_path',
               metavar = 'STR',
               help    = 'Path to the txt files with measurement data.')
   
   p.add_option( '-l','--location',
               type    = 'string',
               default = '',
               dest    = 'location',
               metavar = 'STR',
               help    = 'Location where to put the Bare Module.')
   
   p.add_option( '-i','--ignore_children',
               action  = 'store_true',
               default = False,
               dest    = 'ignore_children',
               help    = 'Do not process the bare component children.')

   p.add_option( '--2fa',
               action  = 'store_true',
               default = False,
               dest    = 'twofa',
               help    = 'Set the two factor authentication login.')

   p.add_option( '--verbose',
               action  = 'store_true',
               default = False,
               dest    = 'verbose',
               help    = 'Force the uploaders to print their configuration and data')
   
   p.add_option( '--debug',
               action  = 'store_true',
               default = False,
               dest    = 'debug',
               help    = 'Force the verbose options in the network query uploaders to print their configuration and data')
   
   p.add_option( '--dev',
               action  = 'store_true',
               default = False,
               dest    = 'isDevelopment',
               help    = 'Set the development database as target.')
   
   p.add_option( '--upload',
               action  = 'store_true',
               default = False,
               dest    = 'upload',
               help    = 'Perform the data upload in database')
   
   
   (opt, args) = p.parse_args()

   if len(args)>2:
      p.error("accepts at most one argument!")


   from rhapi import RhApi
   dburl = 'https://cmsdca.cern.ch/trk_rhapi'
   DBapi = RhApi(dburl, debug=opt.verbose, sso=('login' if not opt.twofa else 'login2') )
   print ('Available DB folders:')
   print(DBapi.folders())
   print()


   BaseUploader.database = 'cmsr' if opt.isDevelopment==False else 'int2r'
   BaseUploader.verbose = opt.verbose
   BaseUploader.debug = opt.debug
   BaseUploader.login = 'login' if not opt.twofa else 'login2'

   modules = UploaderContainer('BareModules')
   pieces  = UploaderContainer('BareModuleComponents')
      
   # gather csv files conatining the data measurements
   data_files = sorted( search_files(opt.data_path,'*.csv') )
   
   nFiles = len(data_files)
   
   print (f'Got {nFiles} csv module files to process!')
   print ()
   
   for i in range(nFiles):
      f = data_files[i]
      sd = TableReader(f, tabSize=43,csv_delimiter=";")
      data = sd.getDataAsCWiseDict()
      
      Wig = [f'Processing modules in {f}: ', Bar('=', '[', ']'), ' ', Percentage()]
      for i in progressbar(range(len(data['Bare module ID'])), widgets=Wig, redirect_stdout=True):
          # Gather the i-esimo bare module data only
          if data['Bare module ID'][i]!='':
              module_name = data['Bare module ID'][i]
              cus_remarks = data['Remarks for customer'][i]
              rework_date = data['RW Date'][i]
              fabric_date = datetime.strptime(data['FC Date'][i], "%d.%m.%y").strftime("%Y-%m-%d")
              
              print (f'Setting up bare module {module_name}')
              bare_module = ITBareModule(module_name,fabric_date,rework_date,cus_remarks,opt.location)
              
              if not opt.ignore_children:
                 bare_module.add_sensor(data['Sensor No.'][i])
                 print (f'  module sensor: ({bare_module.sensor_id}) {bare_module.sensor}')
                 chips = bare_module.number_of_crocs()
              
                 chips_data = { 'ROC wafer':               [roc_name.replace('-', '_') for roc_name in data['ROC wafer'][i:i+chips]],  # fix for the roc name
                                'ROC position':            data['ROC position'][i:i+chips],
                                'ROC_slot':                data['ROC_slot'][i:i+chips],
                                'Module/ROC OK/NOK (1/0)': data['Module/ROC OK/NOK (1/0)'][i:i+chips],
                                 }
                 for c in range(len(chips_data['ROC wafer'])):
                    wafer = chips_data['ROC wafer'][c]
                    position = chips_data['ROC position'][c]
                    slot = chips_data['ROC_slot'][c]
                    fp_ok = chips_data['Module/ROC OK/NOK (1/0)'][c]
                    bare_module.add_croc(wafer,position,slot,fp_ok)
                 
                 for slot, chip in bare_module.crocs.items():
                    if(slot=='0') : print (f'  module chips: {slot} : {chip}')
                    else: print (f'                {slot} : {chip}')

              modules.add(bare_module)
              constituents = bare_module.subitems()
              for piece in constituents: pieces.add(piece)
              
              time.sleep(0.1)
              
   modules_xml = modules.dump_xml_data() if len(modules.uploaders)!=0 else None
   pieces_xml  = pieces.dump_xml_data(modules.uploaders) if len(pieces.uploaders)!=0 else None        

   if opt.upload:
      # The test mode of the DB-Loader does not work for attributes 
      if modules_xml!=None: Upload(modules_xml)
      if pieces_xml!=None: Upload(pieces_xml)


